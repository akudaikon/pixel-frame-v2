#include "freertos/FreeRTOS.h"
#include "esp_err.h"
#include "settings.h"
#include "ini.h"
#include "brightnessctrl.h"

esp_err_t readSettings()
{
  ini_t *config = ini_load("/sd/system/config.ini");
  if (config == NULL) return ESP_FAIL;

  // [wifi]
  if (ini_sget(config, "wifi", "ssid", "%31c", &(settings.wifiSSID)) == 0) return ESP_FAIL;           // required
  if (ini_sget(config, "wifi", "password", "%63c", &(settings.wifiPassword)) == 0) return ESP_FAIL;   // required

  // [mqtt]
  if (ini_bget(config, "mqtt", "enable", &(settings.mqttEnable)) == 0) settings.mqttEnable = false;
  if (ini_sget(config, "mqtt", "uri", "%255c", &(settings.mqttURI)) == 0) settings.mqttEnable = false;
  
  // [settings]
  if (ini_sget(config, "settings", "brightness", "%d", &(settings.maxBrightness)) == 0) settings.maxBrightness = DEFAULT_BRIGHTNESS;
  if (ini_bget(config, "settings", "auto brightness", &(settings.autoBrightness)) == 0) settings.autoBrightness = DEFAULT_AUTOBRIGHT;

  // [defaults]
  if (ini_sget(config, "defaults", "frame time", "%d", &(settings.defaultFrameTime)) == 0) settings.defaultFrameTime = DEFAULT_FRAMETIME;
  if (ini_sget(config, "defaults", "animation time", "%d", &(settings.defaultAnimTime)) == 0) settings.defaultAnimTime = DEFAULT_ANIMTIME;
  if (ini_bget(config, "defaults", "loop", &(settings.defaultLoop)) == 0) settings.defaultLoop = DEFAULT_LOOP;

  if (settings.maxBrightness > BRIGHTNESS_MAX) settings.maxBrightness = BRIGHTNESS_MAX;
  //else if (settings.maxBrightness < BRIGHTNESS_MIN) settings.maxBrightness = BRIGHTNESS_MIN;
  
  settings.brightness = settings.maxBrightness;

  settings.sleepMode = false;
  settings.repeatMode = false;

  ini_free(config);
  return ESP_OK;
}