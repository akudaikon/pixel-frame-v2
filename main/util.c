#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_spiffs.h"
#include "esp_vfs_fat.h"
#include "driver/sdmmc_host.h"
#include "driver/sdspi_host.h"
#include "sdmmc_cmd.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_http_client.h"
#include "esp_ota_ops.h"

#include "eventgroup_defs.h"
#include "ledmatrix.h"
#include "util.h"

esp_err_t initSPIFFS()
{
  esp_vfs_spiffs_conf_t spiffs_conf = 
  {
    .base_path = "/spiffs",
    .partition_label = NULL,
    .max_files = 5,
    .format_if_mount_failed = false,
  };

  esp_err_t ret = esp_vfs_spiffs_register(&spiffs_conf);

  if (ret != ESP_OK)
  {
    if (ret == ESP_FAIL) { ESP_LOGE(__func__, "Failed to mount SPIFFS partition\n"); }
    else if (ret == ESP_ERR_NOT_FOUND) { ESP_LOGE(__func__, "Failed to find SPIFFS partition\n"); }
    else { ESP_LOGE(__func__, "Failed to initialize SPIFFS (%d)\n", ret); }
    return ESP_FAIL;
  }

  ESP_LOGI(__func__, "SPIFFS mounted");
  size_t total = 0, used = 0;
  esp_spiffs_info(NULL, &total, &used);
  ESP_LOGI(__func__, "SPIFFS total: %d, used: %d", total, used);
  xEventGroupSetBits(xEventGroup, SPIFFS_MOUNTED);
  return ESP_OK;
}

esp_err_t initSD()
{
  sdmmc_host_t host = SDMMC_HOST_DEFAULT();
  host.flags = SDMMC_HOST_FLAG_1BIT;
  host.max_freq_khz = SDMMC_FREQ_HIGHSPEED;

  sdmmc_slot_config_t slot_config = SDMMC_SLOT_CONFIG_DEFAULT();

  esp_vfs_fat_sdmmc_mount_config_t mount_config =
  {
    .format_if_mount_failed = false,
    .max_files = 5,
  };

  sdmmc_card_t* card;
  esp_err_t ret = esp_vfs_fat_sdmmc_mount("/sd", &host, &slot_config, &mount_config, &card);

  if (ret != ESP_OK) 
  {
    if (ret == ESP_FAIL) { ESP_LOGE(__func__, "Failed to mount SD card filesystem"); }
    else { ESP_LOGE(__func__, "Failed to initialize SD card (%d)", ret); }
    return ESP_FAIL;
  }

  ESP_LOGI(__func__, "SD card mounted");
  sdmmc_card_print_info(stdout, card);
  xEventGroupSetBits(xEventGroup, SDCARD_MOUNTED);
  return ESP_OK;
}

void drawSPIFFSErr()
{
  pixelColor_t pixelData[MATRIX_HEIGHT][MATRIX_WIDTH] = { 0 };
  pixelColor_t *pixelDataPtr = *pixelData;
  
  // outer box
  for (uint8_t i = 2; i < 28; i++)
  {
    pixelData[2][i].color = 0xFFFFFF;
    pixelData[28][i].color = 0xFFFFFF;
    pixelData[i][2].color = 0xFFFFFF;
    pixelData[i][28].color = 0xFFFFFF;
  }

  // inner box
  for (uint8_t i = 5; i < 22; i++)
  {
    pixelData[5][i].color = 0xFFFFFF;
    pixelData[22][i].color = 0xFFFFFF;
    pixelData[i][5].color = 0xFFFFFF;
    pixelData[i][22].color = 0xFFFFFF;
  }

  // eyes
  pixelData[13][10].color = 0xFFFFFF;
  pixelData[13][12].color = 0xFFFFFF;
  pixelData[13][19].color = 0xFFFFFF;
  pixelData[13][21].color = 0xFFFFFF;
  pixelData[14][11].color = 0xFFFFFF;
  pixelData[14][13].color = 0xFFFFFF;
  pixelData[15][10].color = 0xFFFFFF;
  pixelData[15][12].color = 0xFFFFFF;
  pixelData[15][19].color = 0xFFFFFF;

  // mouth
  pixelData[18][14].color = 0xFFFFFF;
  pixelData[18][15].color = 0xFFFFFF;
  pixelData[18][16].color = 0xFFFFFF;
  pixelData[18][17].color = 0xFFFFFF;
  pixelData[19][13].color = 0xFFFFFF;
  pixelData[19][18].color = 0xFFFFFF;

  // send to LED matrix
  xQueueSend(ledMatrixQueue, &pixelDataPtr, portMAX_DELAY);
}

esp_err_t downloadFileToSD(char* url)
{
  // extract file name from URL
  char* fileName;
  char* fileNamePtr = strrchr(url, '/');
  if (fileNamePtr == NULL) 
  {
    ESP_LOGE(__func__, "Error extracting filename from URL");
    return ESP_FAIL;
  }
  fileName = calloc(fileNamePtr - url, sizeof(char));
  strncpy(fileName, fileNamePtr + 1, strlen(fileNamePtr + 1));

  *fileNamePtr = '\0';  // remove '/' for second strrchr

  // extract animation folder name from URL
  char* pathName;
  char* pathPtr = strrchr(url, '/');
  if (pathPtr == NULL)
  {
    ESP_LOGE(__func__, "Error extracting path from URL");
    free(fileName);
    return ESP_FAIL;
  }
  pathName = calloc(pathPtr - url, sizeof(char));
  strncpy(pathName, pathPtr + 1, strlen(pathPtr + 1));

  *fileNamePtr = '/';  // replace '/'

  // construct full path for SD
  char* fullPath = calloc(4 + strlen(pathName) + 1 + strlen(fileName) + 1, sizeof(char));
  strcpy(fullPath, "/sd/");
  strcat(fullPath, pathName);

  // connect to server
  esp_http_client_config_t config = { .url = url };
  esp_http_client_handle_t client = esp_http_client_init(&config);
  esp_http_client_set_method(client, HTTP_METHOD_GET);
  if (esp_http_client_open(client, 0) != ESP_OK)
  {
    ESP_LOGE(__func__, "Error connecting to server, %s", url);
    free(fileName);
    free(pathName);
    free(fullPath);
    esp_http_client_cleanup(client);
    return ESP_FAIL;
  }

  // get headers (and content length)
  int contentLength = esp_http_client_fetch_headers(client);
  int bytesLeft = contentLength;

  // make sure we got a 200 response
  if (esp_http_client_get_status_code(client) != 200)
  {
    ESP_LOGE(__func__, "Bad server response");
    free(fileName);
    free(pathName);
    free(fullPath);
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return ESP_FAIL;
  }

  // check if animation folder doesn't exist on SD, create it
  DIR *dirPtr;
  dirPtr = opendir(fullPath);
  if (dirPtr == NULL) mkdir(fullPath, 0777);
  else closedir(dirPtr);

  // open file we're creating for writing
  strcat(fullPath, "/");
  strcat(fullPath, fileName);
  FILE *filePtr = fopen(fullPath, "w");
  if (filePtr == NULL)
  {
    ESP_LOGE(__func__, "Error opening file on SD");
    free(fileName);
    free(pathName);
    free(fullPath);
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return ESP_FAIL;
  }

  // malloc receive buffer
  char *buffer = malloc(HTTP_RECV_BUFFER + 1);
  if (buffer == NULL)
  {
    ESP_LOGE(__func__, "Unable to malloc receive buffer");
    free(fileName);
    free(pathName);
    free(fullPath);
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return ESP_FAIL;
  }

  // read data from server and write to file
  int readLen;
  while (bytesLeft > 0)
  {
    readLen = esp_http_client_read(client, buffer, HTTP_RECV_BUFFER);

    if (readLen <= 0)
    {
      ESP_LOGE(__func__, "Got read length of 0");
      free(fileName);
      free(pathName);
      free(fullPath);
      esp_http_client_close(client);
      esp_http_client_cleanup(client);
      return ESP_FAIL;
    }

    fwrite(buffer, sizeof(char), readLen, filePtr);
    bytesLeft -= readLen;
  }

  // cleanup!
  esp_http_client_close(client);
  esp_http_client_cleanup(client);

  fclose(filePtr);

  free(buffer);
  free(fullPath);
  free(pathName);
  free(fileName);

  return ESP_OK;
}

esp_err_t deleteFolderFromSD(char* folder)
{
  char *path;
  char *filePath; 
  DIR *dirPtr;
  struct dirent* entry;

  // construct full path for SD
  path = calloc(4 + strlen(folder) + 1, sizeof(char));
  strcpy(path, "/sd/");
  strcat(path, folder);

  // open directory
  dirPtr = opendir(path);
  if (dirPtr == NULL)
  {
    free(path);
    return ESP_FAIL;
  }

  // delete all files in directory
  while ((entry = readdir(dirPtr)) != NULL)
  {
    if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

    filePath = calloc(strlen(path) + strlen(entry->d_name) + 2, sizeof(char));
    strcpy(filePath, path);
    strcat(filePath, "/");
    strcat(filePath, entry->d_name);

    remove(filePath);
    free(filePath);
  }
  closedir(dirPtr);

  // finally, delete the directory
  if (remove(path) != 0)
  {
    free(path);
    return ESP_FAIL;
  }

  free(path);
  return ESP_OK;
}

const char* getFirmwareVersion()
{
  const esp_app_desc_t* appDesc = esp_ota_get_app_description();
  return appDesc->version;
}

esp_err_t otaUpdate(char* url, bool spiffs)
{
  esp_http_client_config_t config = { .url = url };
  esp_http_client_handle_t client = esp_http_client_init(&config);

  // connect to server
  if (esp_http_client_open(client, 0) != ESP_OK)
  {
    ESP_LOGE(__func__, "Error connecting to server, %s", url);
    esp_http_client_cleanup(client);
    return ESP_FAIL;
  }

  // get headers (and content length)
  int contentLength = esp_http_client_fetch_headers(client);
  int bytesLeft = contentLength;

  // make sure we got a 200 response
  if (esp_http_client_get_status_code(client) != 200)
  {
    ESP_LOGE(__func__, "Bad server response");
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return ESP_FAIL;
  }

  esp_ota_handle_t update_handle = 0;
  const esp_partition_t *update_partition = NULL;

  ESP_LOGI(__func__, "Starting OTA...");

  if (spiffs) update_partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_SPIFFS, NULL);
  else update_partition = esp_ota_get_next_update_partition(NULL);

  if (update_partition == NULL) 
  {
    ESP_LOGE(__func__, "OTA partition not found");
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return ESP_FAIL;
  }

  ESP_LOGI(__func__, "Writing to partition subtype %d at offset 0x%x", update_partition->subtype, update_partition->address);

  esp_err_t err = esp_ota_begin(update_partition, OTA_SIZE_UNKNOWN, &update_handle);
  if (err != ESP_OK)
  {
    ESP_LOGE(__func__, "esp_ota_begin failed, error=%d", err);
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return ESP_FAIL;
  }

  ESP_LOGI(__func__, "esp_ota_begin succeeded");

  char *buffer = malloc(OTA_BUF_SIZE);
  if (buffer == NULL)
  {
    ESP_LOGE(__func__, "Unable to malloc receive buffer");
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return ESP_FAIL;
  }

  // read data from server and write to OTA
  int readLen;
  esp_err_t ota_write_err = ESP_OK;
  while (bytesLeft > 0)
  {
    readLen = esp_http_client_read(client, buffer, OTA_BUF_SIZE);

    if (readLen <= 0)
    {
      ESP_LOGE(__func__, "Got read length < 0");
      esp_http_client_close(client);
      esp_http_client_cleanup(client);
      return ESP_FAIL;
    }

    ota_write_err = esp_ota_write(update_handle, (const void *)buffer, readLen);
    if (ota_write_err != ESP_OK) break;

    bytesLeft -= readLen;
  }
  free(buffer);
  esp_http_client_close(client);
  esp_http_client_cleanup(client);

  esp_err_t ota_end_err = esp_ota_end(update_handle);
  if (ota_write_err != ESP_OK)
  {
    ESP_LOGE(__func__, "Error: esp_ota_write failed! err=0x%d", err);
    return ESP_FAIL;
  } 
  else if (ota_end_err != ESP_OK)
  {
    ESP_LOGE(__func__, "Error: esp_ota_end failed! err=0x%d. Image is invalid", ota_end_err);
    return ESP_FAIL;
  }

  if (esp_ota_set_boot_partition(update_partition) != ESP_OK) 
  {
    ESP_LOGE(__func__, "esp_ota_set_boot_partition failed! err=0x%d", err);
    return ESP_FAIL;
  }

  ESP_LOGI(__func__, "OTA successful!"); 
  return ESP_OK;
}