#ifndef EVENTGROUP_DEFS_H
#define EVENTGROUP_DEFS_H

#define LEDMATRIX_TASK_STARTED   BIT0
#define DIRECTOR_TASK_STARTED    BIT1
#define SDCARD_MOUNTED           BIT2
#define SPIFFS_MOUNTED           BIT3
#define BRIGHTNESS_TASK_STARTED  BIT4
#define HTTP_TASK_STARTED        BIT5
#define WEBSOCKETS_TASK_STARTED  BIT6
#define MQTT_TASK_STARTED        BIT7

#define DIRECTOR_CMD_OK          BIT8
#define DIRECTOR_CMD_FAIL        BIT9

#define WIFI_CONNECTED           BIT10
#define WIFI_DISCONNECTED        BIT11
#define MQTT_CONNECTED           BIT12

#define TOUCH_EVENT              BIT14
#define TOUCH_LONG_EVENT         BIT15

extern EventGroupHandle_t xEventGroup;

#endif
