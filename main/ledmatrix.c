#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "driver/gpio.h"
#include "soc/gpio_struct.h"
#include "esp_log.h"
#include "esp_heap_caps.h"
#include <string.h>

#include "eventgroup_defs.h"
#include "pins.h"
#include "i2s_parallel.h"
#include "director.h"
#include "ledmatrix.h"
#include "settings.h"

static i2s_parallel_buffer_desc_t bufdesc[NUM_FRAME_BUFFERS][MATRIX_HEIGHTDIV2 + 1][1 << (NUM_BITPLANES - LSBMSB_TRANSITION_BIT - 1)];

static i2s_parallel_config_t cfg =
{
  .gpio_bus = { LED_R1_PIN,
                LED_G1_PIN,
                LED_B1_PIN,
                LED_R2_PIN,
                LED_G2_PIN,
                LED_B2_PIN,
                -1,
                -1,
                LED_A_PIN,
                LED_B_PIN,
                LED_C_PIN,
                LED_D_PIN,
                LED_LAT_PIN,
                LED_OE_PIN,
                -1,
                -1 },

  .gpio_clk = LED_CLK_PIN,
  .bits = I2S_PARALLEL_BITS_16,
  .clkspeed_hz = 20000000,  // 20MHz
  .bufa = bufdesc[0][0],
  .bufb = bufdesc[1][0]
};

void ledMatrix_task(void *pvParameters)
{
  frameStruct_t *bitplane;
  bitplane = (frameStruct_t*)heap_caps_malloc(sizeof(frameStruct_t) * NUM_FRAME_BUFFERS, MALLOC_CAP_DMA);
  assert("Can't allocate bitplane memory");
  
  memset(bitplane, 0, sizeof(frameStruct_t) * NUM_FRAME_BUFFERS);

  for (uint8_t j = 0; j < MATRIX_HEIGHTDIV2; j++)
  {
    // first set of data is LSB through MSB, single pass - all color bits are displayed once, 
    // which takes care of everything below and including LSBMSB_TRANSITION_BIT
    bufdesc[0][j][0].memory = bitplane[0].rowdata[j].rowbits[0].data; 
    bufdesc[0][j][0].size = sizeof(rowBitStruct_t) * NUM_BITPLANES;
    bufdesc[1][j][0].memory = bitplane[1].rowdata[j].rowbits[0].data; 
    bufdesc[1][j][0].size = sizeof(rowBitStruct_t) * NUM_BITPLANES;

    uint8_t nextBufdescIndex = 1;

    for (uint8_t i = LSBMSB_TRANSITION_BIT + 1; i < NUM_BITPLANES; i++) 
    {
      // binary time division setup: we need 2 of bit (LSBMSB_TRANSITION_BIT + 1) four of (LSBMSB_TRANSITION_BIT + 2), etc
      // because we sweep through to MSB each time, it divides the number of times we have to sweep in half
      // we need 2^(i - LSBMSB_TRANSITION_BIT - 1) == 1 << (i - LSBMSB_TRANSITION_BIT - 1) passes from i to MSB
      for (uint8_t k = 0; k < 1 << (i - LSBMSB_TRANSITION_BIT - 1); k++) 
      {
        bufdesc[0][j][nextBufdescIndex].memory = bitplane[0].rowdata[j].rowbits[i].data;
        bufdesc[0][j][nextBufdescIndex].size = sizeof(rowBitStruct_t) * (NUM_BITPLANES - i);
        bufdesc[1][j][nextBufdescIndex].memory = bitplane[1].rowdata[j].rowbits[i].data;
        bufdesc[1][j][nextBufdescIndex].size = sizeof(rowBitStruct_t) * (NUM_BITPLANES - i);
        nextBufdescIndex++;
      }
    }
  }
  // end marker
  bufdesc[0][MATRIX_HEIGHTDIV2][0].memory = NULL;
  bufdesc[1][MATRIX_HEIGHTDIV2][0].memory = NULL;

  // setup I2S
  uint8_t inactiveBufID = 0;
  i2s_parallel_setup(&I2S1, &cfg);

  ledMatrixQueue = xQueueCreate(1, sizeof(pixelColor_t *));

  ESP_LOGI(__func__, "LED matrix driver task started on core %d", xPortGetCoreID());
  xEventGroupSetBits(xEventGroup, LEDMATRIX_TASK_STARTED);
  
  while (1)
  {
    pixelColor_t *pixelDataPtr = NULL;

    // if we receive new pixel data, update I2S buffer with data
    if (xQueueReceive(ledMatrixQueue, &pixelDataPtr, portMAX_DELAY))
    {
      for (uint8_t y = 0; y < MATRIX_HEIGHTDIV2; y++)
      {
        for (uint8_t pl = 0; pl < NUM_BITPLANES; pl++) 
        {
          uint16_t *p = bitplane[inactiveBufID].rowdata[y].rowbits[pl].data;
          uint16_t mask = (1 << (8 - NUM_BITPLANES + pl));
          uint16_t lbits = 0;
          uint8_t rowAddress = y;

          if (pl == 0) rowAddress = y - 1;  // special case for LSB, output previous row's (as previous row is being displayed for one latch cycle)

          if (rowAddress & 1) lbits |= I2S_BIT_A;
          if (rowAddress & 2) lbits |= I2S_BIT_B;
          if (rowAddress & 4) lbits |= I2S_BIT_C;
          if (rowAddress & 8) lbits |= I2S_BIT_D;

          for (uint8_t fx = 0; fx < MATRIX_WIDTH; fx++)
          {
            uint8_t x = fx;
            uint16_t v = lbits;

            // turn off OE while the line bits are changing
            if (fx < OE_OFF_CLKS_AFTER_LATCH || fx >= (MATRIX_WIDTH - 1)) v|= I2S_BIT_OE;

            // turn off OE after brightness value is reached - used for MSBs and LSB
            // MSBs always output normal brightness
            // LSB outputs normal brightness as MSB from previous row is being displayed
            if ((pl > LSBMSB_TRANSITION_BIT || !pl) && fx > settings.brightness + OE_OFF_CLKS_AFTER_LATCH) v |= I2S_BIT_OE;

            // special case for the bits *after* LSB through (LSBMSB_TRANSITION_BIT)
            // OE is output after data is shifted, so need to set OE to fractional brightness
            if (pl && pl <= LSBMSB_TRANSITION_BIT) 
            {
              // divide brightness in half for each bit below LSBMSB_TRANSITION_BIT
              uint8_t lsbBrightness = settings.brightness >> (LSBMSB_TRANSITION_BIT - pl + 1);
              if (fx > lsbBrightness + OE_OFF_CLKS_AFTER_LATCH) v |= I2S_BIT_OE;
            }

            if (fx == MATRIX_WIDTH - 1) v|= I2S_BIT_LAT; // latch on last bit

            if (pixelDataPtr[y + (x * MATRIX_HEIGHT)].red & mask)                         v |= I2S_BIT_R1;
            if (pixelDataPtr[y + (x * MATRIX_HEIGHT)].green & mask)                       v |= I2S_BIT_G1;
            if (pixelDataPtr[y + (x * MATRIX_HEIGHT)].blue & mask)                        v |= I2S_BIT_B1;
            if (pixelDataPtr[(y + MATRIX_HEIGHTDIV2) + (x * MATRIX_HEIGHT)].red & mask)   v |= I2S_BIT_R2;
            if (pixelDataPtr[(y + MATRIX_HEIGHTDIV2) + (x * MATRIX_HEIGHT)].green & mask) v |= I2S_BIT_G2;
            if (pixelDataPtr[(y + MATRIX_HEIGHTDIV2) + (x * MATRIX_HEIGHT)].blue & mask)  v |= I2S_BIT_B2;

            // save the calculated value to the bitplane memory in reverse order to account for I2S Tx FIFO mode1 ordering
            if (fx % 2)
            {
              *p = v;
              p += 2;
            } 
            else *(p + 1) = v;
          }
        }
      }

      i2s_parallel_flip_to_buffer(&I2S1, inactiveBufID);
      inactiveBufID ^= 1;
    }
  }
}