#ifndef DIRECTOR_H
#define DIRECTOR_H

#include "freertos/queue.h"

#define MAX_PATH_LENGTH   512

#define MIN_FRAME_TIME    10
#define FRAME_TIME_OFFSET 10

#define UPDATE_HELLO          BIT0
#define UPDATE_IP_VERSION     BIT1
#define UPDATE_BRIGHT_MODES   BIT2
#define UPDATE_CUR_NEXT_ANIM  BIT3

QueueHandle_t directorCmdQueue;
enum directorCmd
{
  START_DIRECTING,
  STOP_DIRECTING,
  GOTO_SLEEP,
  PLAY_NEXT,
  SET_BRIGHTNESS,
  SET_REPEAT,
  SEND_UPDATE,
  DOWNLOAD_FILE,
  DELETE_ANIM,
  REINDEX_ANIMS,
  UPDATE_FIRMWARE,
  UPDATE_SPIFFS,
  RESTART,
};

typedef struct directorCmd_t
{
  uint8_t cmd;
  union
  {
    bool      paramBool;
    uint32_t  paramInt;
    char      paramStr[MAX_PATH_LENGTH];
  };
} directorCmd_t;

typedef struct animationState_t
{
  char      path[MAX_PATH_LENGTH];
  uint32_t  frameTime;
  uint32_t  animTime;
  uint16_t  curFrame;
  bool      loop;
} animationState_t;

typedef struct bmpHeader_t
{
  uint16_t  signature;
  uint32_t  filesize;
  uint16_t  reserved_0;
  uint16_t  reserved_1;
  uint32_t  offset;
  uint32_t  size;
  int32_t   width;
  int32_t   height;
  uint16_t  n_cplanes;
  uint16_t  n_bpp;
  uint32_t  compression;
  uint32_t  img_size;
  int32_t   y_res;
  int32_t   x_res;
  uint32_t  n_cpalette;
  uint32_t  n_important;
} __attribute__((packed)) bmpHeader_t;

void drawBitmap(char *filename);
void director_startDirecting();
void director_stopDirecting();
void director_sleep(bool enable);
void director_playNext(char* file);
void director_setBrightness(uint8_t brightness);
void director_setRepeat(bool enable);
void director_sendUpdate(uint8_t updateBits);
void director_reindexAnims();
esp_err_t director_downloadFile(char* file);
esp_err_t director_deleteAnim(char* name);
esp_err_t director_updateSPIFFS(char* url);
esp_err_t director_updateFirmware(char* url);
void director_restart();

void director_task(void *pvParameters);

#endif