#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "driver/touch_pad.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/sens_reg.h"

#include "eventgroup_defs.h"
#include "director.h"
#include "settings.h"
#include "touch.h"

// TODO: Might not need filtering since we're also debouncing?
// TODO: Probably need to update threshold periodically?

#define TOUCH_CHANNEL         0
#define TOUCH_FILTER_PERIOD   10
#define TOUCH_THRESH_PERCENT  66

#define LONG_PRESS_MS         3000

static bool taskStop = false;

static uint16_t touchThresh = 0;
static int8_t debounceCount = 0;
static bool debouncedState = false;
static bool isTouched = false;
static bool processed = false;

static TickType_t touchTime;

static void setTouchThresh()
{
  touch_pad_read_filtered(TOUCH_CHANNEL, &touchThresh);
  touchThresh *= (TOUCH_THRESH_PERCENT / 100);
}

void touch_task(void *pvParameters)
{
  touch_pad_init();
  touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V);
  touch_pad_config(TOUCH_CHANNEL, 0);
  touch_pad_filter_start(TOUCH_FILTER_PERIOD);
  setTouchThresh();

  ESP_LOGI(__func__, "Touch task started on core %d", xPortGetCoreID());
  //xEventGroupSetBits(xEventGroup, BUTTON_TASK_STARTED);

  while (!taskStop)
  {
    uint16_t value = 0;
    touch_pad_read_filtered(TOUCH_CHANNEL, &value);

    // Button debouncing (buttons are active-low)
    if (value < touchThresh)
    {
      if (++debounceCount > 3)
      {
        debounceCount = 3;
        debouncedState = true;
      }
    }
    else
    {
      if (--debounceCount < -3)
      {
        debounceCount = -3;
        debouncedState = false;
      }
    }

    // Touch button touched
    if (debouncedState == true)
    {
      // Just touched
      if (!isTouched)
      {
        touchTime = xTaskGetTickCount();
        isTouched = true;
      }
      else
      {
        // If touch button held for LONG_PRESS_MS...
        if (!processed && xTaskGetTickCount() - touchTime > pdMS_TO_TICKS(LONG_PRESS_MS))
        {
          xEventGroupSetBits(xEventGroup, TOUCH_LONG_EVENT);
          processed = true;
        }
      }
    }
    // Touch button released
    else if (isTouched)
    {
      if (!processed) xEventGroupSetBits(xEventGroup, TOUCH_EVENT);
      isTouched = false;
      processed = false;
    }

    vTaskDelay(10 / portTICK_PERIOD_MS);
  }
}

void touch_processTouch()
{
  if (xEventGroupGetBits(xEventGroup) & TOUCH_EVENT)
  {
    xEventGroupClearBits(xEventGroup, TOUCH_EVENT);
    director_playNext("");
  }
  else if (xEventGroupGetBits(xEventGroup) & TOUCH_LONG_EVENT)
  {
    xEventGroupClearBits(xEventGroup, TOUCH_LONG_EVENT);
    director_sleep(!settings.sleepMode);
  }
}

void touch_stop()
{
  taskStop = true;
}