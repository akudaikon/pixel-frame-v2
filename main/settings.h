#ifndef SETTINGS_H
#define SETTINGS_H

#define DEFAULT_AUTOBRIGHT  true
#define DEFAULT_BRIGHTNESS  32
#define DEFAULT_FRAMETIME   250
#define DEFAULT_ANIMTIME    60000
#define DEFAULT_LOOP        true

typedef struct settingsData_t
{
  char ipAddress[16];
  uint8_t brightness;
  bool sleepMode;
  bool repeatMode;

  // [wifi]
  char wifiSSID[32];
  char wifiPassword[64];

  // [mqtt]
  bool mqttEnable;
  char mqttURI[256];

  // [settings]
  bool autoBrightness;
  uint8_t maxBrightness;

  // [defaults]
  uint32_t defaultFrameTime;
  uint32_t defaultAnimTime;
  bool defaultLoop;
} settingsData_t;

extern settingsData_t settings;

esp_err_t readSettings();

#endif