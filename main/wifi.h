#ifndef WIFI_H
#define WIFI_H

#include "esp_err.h"

#define WIFI_CONNECT_TIMEOUT   15000  // 15 sec
#define WIFI_RECONNECT_DELAY   30000  // 30 sec

esp_err_t wifiConnect(char* ssid, char* password);
void wifiDisconnect();

#endif
