#ifndef UTIL_H
#define UTIL_H

#include "esp_err.h"

#define HTTP_RECV_BUFFER 512
#define OTA_BUF_SIZE     256

esp_err_t initSPIFFS();
esp_err_t initSD();
void drawSPIFFSErr();
esp_err_t downloadFileToSD(char* url);
esp_err_t deleteFolderFromSD(char* folder);
const char* getFirmwareVersion();
esp_err_t otaUpdate(char* url, bool spiffs);

#endif