#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"

#include "sdkconfig.h"
#include "wifi.h"
#include "settings.h"
#include "eventgroup_defs.h"

static esp_timer_handle_t timer_reconnect;

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
  switch(event->event_id)
  {
    case SYSTEM_EVENT_STA_START:
      ESP_LOGI(__func__, "started");
      esp_wifi_connect();
      break;
    case SYSTEM_EVENT_STA_CONNECTED:
      ESP_LOGI(__func__, "connected");
      break;
    case SYSTEM_EVENT_STA_GOT_IP:
      strcpy(settings.ipAddress, ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
      xEventGroupSetBits(xEventGroup, WIFI_CONNECTED);
      break;
    case SYSTEM_EVENT_STA_STOP:
      ESP_LOGI(__func__, "stopped");
      break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
      ESP_LOGI(__func__, "disconnected. reason: %d", event->event_info.disconnected.reason);
      xEventGroupClearBits(xEventGroup, WIFI_CONNECTED);
      xEventGroupSetBits(xEventGroup, WIFI_DISCONNECTED);
      esp_timer_start_once(timer_reconnect, WIFI_RECONNECT_DELAY * 1000);
      break;
    default:
      break;
  }
  return ESP_OK;
}

esp_err_t wifiConnect(char* ssid, char* password)
{
  nvs_flash_init();
  tcpip_adapter_init();
  esp_event_loop_init(event_handler, NULL);

  esp_timer_create_args_t timerConfig;
  timerConfig.dispatch_method = ESP_TIMER_TASK;
  timerConfig.callback = (void*)esp_wifi_connect;
  timerConfig.arg = 0;
  timerConfig.name = "reconnect";
  esp_timer_create(&timerConfig, &timer_reconnect);

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  esp_wifi_init(&cfg);

  esp_wifi_set_storage(WIFI_STORAGE_RAM);

  wifi_config_t wifi_config;
  memset(&wifi_config, 0, sizeof(wifi_config));
  memcpy(wifi_config.sta.ssid, ssid, sizeof(wifi_config.sta.ssid));
  memcpy(wifi_config.sta.password, password, sizeof(wifi_config.sta.password));

  esp_wifi_set_mode(WIFI_MODE_STA);
  esp_wifi_set_config(WIFI_IF_STA, &wifi_config);
  xEventGroupClearBits(xEventGroup, WIFI_CONNECTED | WIFI_DISCONNECTED);
  esp_wifi_start();

  ESP_LOGI(__func__, "Connecting to WiFi - %s %s", wifi_config.sta.ssid, wifi_config.sta.password);

  return ESP_OK;
}

void wifiDisconnect()
{
  esp_wifi_disconnect();
  esp_wifi_stop();

  ESP_LOGI(__func__, "Disconnected from WiFi");
}