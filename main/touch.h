#ifndef TOUCH_H
#define TOUCH_H

void touch_task(void *pvParameters);
void touch_processTouch();
void touch_stop();

#endif