var repoUrl = "https://api.bitbucket.org/2.0/repositories/akudaikon/pixelframerepo/src/master/"
var imageUrlBase = "https://bitbucket.org/akudaikon/pixelframerepo/raw/master/"

var pixelFrameAnims = [],
    repoAnims = [],
    updateItemCount = 0,
    newItemCount = 0,
    downloadedItemCount = 0,
    logoPixels = document.getElementsByClassName("logo-pixel"),
    newTable = document.getElementById('newTable'),
    updateTable = document.getElementById('updateTable'),
    downloadedTable = document.getElementById('downloadedTable'),
    updateCount = document.getElementById('updateCount'),
    newCount = document.getElementById('newCount'),
    downloadedCount = document.getElementById('downloadedCount'),
    updateButton = document.getElementById('updateBtn'),
    downloadButton = document.getElementById('downloadBtn'),
    deleteButton = document.getElementById('deleteBtn');

var connection;
var targetLocation = window.location.hostname || "pixelframe.local"
var wsServer = "ws://" + targetLocation + "/websocket";

genLogoPixels = function()
{
  for (var i = 0; i < logoPixels.length; i++) logoPixels[i].style.backgroundColor = 'hsl(' + (Math.random() * 360) + ',100%,' + (Math.random() * 100) + '%)';
  setTimeout(genLogoPixels, 3000);
};

openWebSocket = function()
{
  return new Promise(function(resolve, reject)
  {
    connection = new WebSocket(wsServer);

    connection.onopen = function()
    {
      console.log("CONNECTED");
      resolve(connection);
    };

    connection.onerror = function(err)
    {
      reject(err);
    };
  });
}

getPixelFrameData = function()
{
  return new Promise(function(resolve, reject)
  {
    connection.send("L");

    connection.onmessage = function(evt)
    {
      var msgs = evt.data.split(",");
      for (var i = 0; i < msgs.length; i++) 
      {
        var cmd = msgs[i].charAt(0);

        if (cmd == "L")
        {
          if (msgs[i].charCodeAt(1) == 0x04) // EOT
          {
            downloadedCount.innerHTML = Object.keys(pixelFrameAnims).length;
            resolve();
          }
          else
          {
            var anim = msgs[i].substring(1).split("|");
            var version = anim[1].trim();
            if (version == "") version = 1;
            pixelFrameAnims[anim[0]] = version;
          }
        }
      }
    };

    connection.onerror = function(err)
    {
      reject(err);
    };
  });
}

getRepoData = async function()
{
  try {
    var curRepoUrl = repoUrl + "?pagelen=100";
    while(1)
    {
      repoResp = await fetch(curRepoUrl, {cache: "no-store"});
      repoJSON = await repoResp.json();
      for (i = 0; i < repoJSON['values']['length']; i++) { repoAnims.push(repoJSON['values'][i]['path']); }
      if ('next' in repoJSON) { curRepoUrl = repoJSON['next'] + "&pagelen=100"; } else { break; }
    }
    return repoAnims;
  }
  catch { throw(err); }
}

repoUpdateExists = async function(animationName)
{
  var path = animationName + "/config.ini";
  try {
    var response = await fetch(imageUrlBase + path, {cache: "no-store"}).then(response => { return response.text(); });

    version = /version=(\d+)/g.exec(response);
    if (version && parseInt(version[1]) > pixelFrameAnims[animationName]) return true;
    else return false;
  }
  catch { return false; }
}

populateTables = async function()
{
  // populate new and updated tables
  for (var d = 0; d < repoAnims.length; d++)
  {
    // new animations
    if (!(repoAnims[d] in pixelFrameAnims))
    {
      var wrapper = document.createElement('div');
      wrapper.classList.add('col-xs-3', 'col-sm-2', 'col-md-2', 'col-lg-1');
  
      var row = document.createElement('div');
      row.classList.add('row', 'text-center');
  
      var check = document.createElement('input');
      check.type = "checkbox";
      check.id = 'n_' + repoAnims[d];
      check.classList.add("new");
      row.appendChild(check);
  
      var label = document.createElement("Label");
      label.setAttribute("for", 'n_' + repoAnims[d]);
      label.style.background = "url('" + imageUrlBase + repoAnims[d] + "/0.bmp') no-repeat center";
      label.style.backgroundSize = "cover";
      row.appendChild(label);
  
      var text = document.createElement('div');
      text.classList.add('row', 'text-center', 'animTitle');
      text.innerHTML = repoAnims[d];
  
      wrapper.appendChild(row);
      wrapper.appendChild(text);
      newTable.appendChild(wrapper);

      newItemCount = newItemCount + 1;
    }
    // already downloaded, but updated version exists
    else if (await repoUpdateExists(repoAnims[d]))
    {
      var wrapper = document.createElement('div');
      wrapper.classList.add('col-xs-3', 'col-sm-2', 'col-md-2', 'col-lg-1');
  
      var row = document.createElement('div');
      row.classList.add('row', 'text-center');
  
      var check = document.createElement('input');
      check.type = "checkbox";
      check.id = 'u_' + repoAnims[d];
      check.classList.add("new");
      row.appendChild(check);
  
      var label = document.createElement("Label");
      label.setAttribute("for", 'u_' + repoAnims[d]);
      label.style.background = "url('" + imageUrlBase + repoAnims[d] + "/0.bmp') no-repeat center";
      label.style.backgroundSize = "cover";
      row.appendChild(label);
  
      var text = document.createElement('div');
      text.classList.add('row', 'text-center', 'animTitle');
      text.innerHTML = repoAnims[d];
  
      wrapper.appendChild(row);
      wrapper.appendChild(text);
      updateTable.appendChild(wrapper);

      updateItemCount = updateItemCount + 1;
    }
  }

  // populate downloaded table
  for (animation in pixelFrameAnims)
  {
    var wrapper = document.createElement('div');
    wrapper.classList.add('col-xs-3', 'col-sm-2', 'col-md-2', 'col-lg-1');

    var row = document.createElement('div');
    row.classList.add('row', 'text-center');

    var check = document.createElement('input');
    check.type = "checkbox";
    check.id = 'd_' + animation;
    check.classList.add("downloaded");
    row.appendChild(check);

    var label = document.createElement("Label");
    label.setAttribute("for", 'd_' + animation);
    label.style.background = "url('http://" + targetLocation + "/sd/" + animation + "/0.bmp') no-repeat center";
    label.style.backgroundSize = "cover";
    row.appendChild(label);

    var text = document.createElement('div');
    text.classList.add('row', 'text-center', 'animTitle');
    text.innerHTML = animation;

    wrapper.appendChild(row);
    wrapper.appendChild(text);
    downloadedTable.appendChild(wrapper);
    downloadedItemCount = downloadedItemCount + 1;
  }
}

downloadToPixelFrame = async function(url)
{
  return new Promise(function(resolve, reject)
  {
    connection.send("D" + url);

    connection.onmessage = function(evt)
    {
      console.log(evt.data);
      if (evt.data == "DOK") resolve();
      else if (evt.data == "DERR") reject();
    }
  });
}

deleteFromPixelFrame = async function(animation)
{
  return new Promise(function(resolve, reject)
  {
    connection.send("X" + animation);

    connection.onmessage = function(evt)
    {
      console.log(evt.data);
      if (evt.data == "XOK") resolve();
      else if (evt.data == "XERR") reject();
    }
  });
}

reindexPixelFrame = async function()
{
  return new Promise(function(resolve, reject)
  {
    connection.send("+");

    connection.onmessage = function(evt)
    {
      console.log(evt.data);
      if (evt.data == "+OK") resolve();
    }
  });
}

updateAnimations = async function()
{
  var updates = document.querySelectorAll('#updateTable input[type="checkbox"]:checked');
  
  document.getElementById("progressModalTitle").innerHTML = "<span class='glyphicon glyphicon-save'></span> Updating animations...";
  $('#progressModal').modal('show');

  for (var i = 0; i < updates.length; i++)
  {
    var animFiles = [];
    var updateName = updates[i].id.substring(2);
    document.getElementById("animDetails").innerHTML = "<b>Animation " + (i + 1) + " of " + updates.length + ":</b> " + updateName;

    var curDownloadUrl = repoUrl + updateName + "?pagelen=100";
    while(1)
    {
      repoResp = await fetch(curDownloadUrl, {cache: "no-store"});
      repoJSON = await repoResp.json();
      for (i = 0; i < repoJSON['values']['length']; i++) { animFiles.push(repoJSON['values'][i]['escaped_path']); }
      if ('next' in repoJSON) { curDownloadUrl = repoJSON['next'] + "&pagelen=100"; } else { break; }
    }

    for (var f = 0; f < animFiles.length; f++)
    {
      var updatePercent = Math.round(((f + 1) / animFiles.length) * 100);
      $('#modalProgress').css({width: updatePercent + '%'});
      $('#modalProgress').text(updatePercent + '%');

      await deleteFromPixelFrame(updateName);
      await downloadToPixelFrame(imageUrlBase + animFiles[f]);
    }
  }

  await reindexPixelFrame();
  $('#progressModal').modal('hide');
  await refreshData(false);
}

downloadAnimations = async function()
{
  var downloads = document.querySelectorAll('#newTable input[type="checkbox"]:checked');
  
  document.getElementById("progressModalTitle").innerHTML = "<span class='glyphicon glyphicon-save'></span> Downloading animations...";
  $('#progressModal').modal('show');

  for (var i = 0; i < downloads.length; i++)
  {
    var animFiles = [];
    var downloadName = downloads[i].id.substring(2);
    document.getElementById("animDetails").innerHTML = "<b>Animation " + (i + 1) + " of " + downloads.length + ":</b> " + downloadName;

    var curDownloadUrl = repoUrl + downloadName + "?pagelen=100";
    while(1)
    {
      repoResp = await fetch(curDownloadUrl, {cache: "no-store"});
      repoJSON = await repoResp.json();
      for (i = 0; i < repoJSON['values']['length']; i++) { animFiles.push(repoJSON['values'][i]['escaped_path']); }
      if ('next' in repoJSON) { curDownloadUrl = repoJSON['next'] + "&pagelen=100"; } else { break; }
    }

    for (var f = 0; f < animFiles.length; f++)
    {
      var downloadPercent = Math.round(((f + 1) / animFiles.length) * 100);
      $('#modalProgress').css({width: downloadPercent + '%'});
      $('#modalProgress').text(downloadPercent + '%');

      if (/[0-9]+\.bmp/.test(animFiles[f]) || /config\.ini/.test(animFiles[f]) || /p32\.gif/.test(animFiles[f]))
      {
        await downloadToPixelFrame(imageUrlBase + animFiles[f]);
      }
    }
  }

  await reindexPixelFrame();
  $('#progressModal').modal('hide');
  await refreshData(false);
}

deleteAnimations = async function()
{
  var deletes = document.querySelectorAll('#downloadedTable input[type="checkbox"]:checked');
  
  document.getElementById("progressModalTitle").innerHTML = "<span class='glyphicon glyphicon-trash'></span> Deleting animations...";
  $('#progressModal').modal('show');

  for (var i = 0; i < deletes.length; i++)
  {
    var deleteName = deletes[i].id.substring(2);
    document.getElementById("animDetails").innerHTML = "<b>Animation " + (i + 1) + " of " + deletes.length + ":</b> " + deleteName;

    var deletePercent = Math.round(((i + 1) / deletes.length) * 100);
    $('#modalProgress').css({width: deletePercent + '%'});
    $('#modalProgress').text(deletePercent + '%');

    await deleteFromPixelFrame(deleteName);
  }

  await reindexPixelFrame();
  $('#progressModal').modal('hide');
  await refreshData(false);
}

selectAll = function(div, value)
{
  var items = document.querySelectorAll('#' + div + ' input[type="checkbox"]');
  for (var i = 0; i < items.length; i++) { items[i].checked = value; }
  $('#' + div).parents(".panel-body").find('button').prop('disabled', !value);
}

$(document).on("click", "input[type='checkbox']", function(event)
{
  $(event.target).parents(".panel-body").find('button').prop('disabled', !$(event.target).parents(".panel-body").find("input[type='checkbox']").is(":checked"));
});

$('.panel-collapse').on('show.bs.collapse', function()
{
  $(this).siblings('.panel-heading').addClass('active');
});

$('.panel-collapse').on('hide.bs.collapse', function()
{
  $(this).siblings('.panel-heading').removeClass('active');
});

$('#deleteModal').on('show.bs.modal', function()
{
  var modal = $(this);
  var deleteCount = $('#downloadedTable input[type=checkbox]:checked').length;
  modal.find('.modal-body #deleteModalCount').text(deleteCount);
});

refreshData = async function(initial)
{
  document.getElementById("messageModalTitle").innerHTML = "<i class='fas fa-clock'></i> Please wait";
  document.getElementById("messageModalText").innerHTML = "<i class='fa fa-spinner fa-spin'></i> Getting animation data...";
  $('#messageModal').modal('show');

  pixelFrameAnims = [];
  repoAnims = [];
  updateItemCount = 0;
  newItemCount = 0;
  downloadedItemCount = 0;

  updateTable.innerHTML = "";
  newTable.innerHTML = "";
  downloadedTable.innerHTML = "";
  updateCount.innerHTML = updateItemCount;
  newCount.innerHTML = newItemCount;
  downloadedCount.innerHTML = downloadedItemCount;

  try { await getPixelFrameData(); }
  catch
  {
    document.getElementById("messageModalTitle").innerHTML = "<i class='fas fa-exclamation-triangle' style='color:firebrick'></i> Error";
    document.getElementById("messageModalText").innerHTML = "Unable to get animation data from Pixel Frame!";
    document.getElementById("messageModalSubText").innerHTML = "Please make sure Pixel Frame is powered on and connected to WiFi.<br>Refresh this page to try again.";
    return;
  }
  
  try { await getRepoData(); }
  catch
  {
    document.getElementById("messageModalTitle").innerHTML = "<i class='fas fa-exclamation-triangle' style='color:firebrick'></i> Error";
    document.getElementById("messageModalText").innerHTML = "Unable to get data from animation repository!";
    document.getElementById("messageModalSubText").innerHTML = "Please refresh this page to try again.";
    return;
  }

  await populateTables();

  updateCount.innerHTML = updateItemCount;
  newCount.innerHTML = newItemCount;
  downloadedCount.innerHTML = downloadedItemCount;

  if (updateItemCount == 0) updateTable.innerHTML = "<h5 class='text-center'><b>Nothing here!</b></h5>";
  else if (initial) $('#updatedCollapse').collapse('show');
  if (newItemCount == 0) newTable.innerHTML = "<h5 class='text-center'><b>Nothing here!</b></h5>";
  else if (initial) $('#newCollapse').collapse('show');
  if (downloadedItemCount == 0) downloadedTable.innerHTML = "<h5 class='text-center'><b>Nothing here!</b></h5>";

  $('#messageModal').modal('hide');
}

genLogoPixels();
openWebSocket().then(function()
{
  refreshData(true);
}).catch(function(err)
{
  document.getElementById("messageModalTitle").innerHTML = "<i class='fas fa-exclamation-triangle' style='color:firebrick'></i> Error";
  document.getElementById("messageModalText").innerHTML = "Unable to connect to Pixel Frame!";
  document.getElementById("messageModalSubText").innerHTML = "Please make sure Pixel Frame is powered on and connected to WiFi.<br>Refresh this page to try again.";
  $('#messageModal').modal('show');
});