var animTableArray = [],
    animTable = document.getElementById('animTable'),
    animCountPanel = document.getElementById('animCountPanel'),
    logoPixels = document.getElementsByClassName("logo-pixel"),
    ipAddress = document.getElementById('ipAddress'),
    firmwareVersion = document.getElementById('firmwareVersion'),
    curImg = document.getElementById('curImg'),
    nextImg = document.getElementById('nextImg'),
    wsDisconnected = document.getElementById('wsDisconnected'),
    wsConnected = document.getElementById('wsConnected'),
    wsConnecting = document.getElementById('wsConnecting'),
    mnuReconnect = document.getElementById('mnuReconnect'),
    mnuUpdateFirmware = document.getElementById('mnuFirmware'),
    nextButton = document.getElementById('nextBtn'),
    repeatButton = document.getElementById('repeatBtn'),
    autoBrightness = document.getElementById('autoBrightness'),
    brightnessDiv = document.getElementById('brightness'),
    brightnessRange = document.getElementById('brightnessRange'),
    sleepMode = document.getElementById('sleepMode'),
    updateButton = document.getElementById('updateBtn'),
    forceUpdateButton = document.getElementById('forceUpdateBtn'),
    pixelFrameVersion, repoVersion;

var connection;
var targetLocation = window.location.hostname || "pixelframe.local"
var wsServer = "ws://" + targetLocation + "/websocket";
var webSocketConnected = false;
var animTableLoaded = false;

genLogoPixels = function() {
  for (var i = 0; i < logoPixels.length; i++) logoPixels[i].style.backgroundColor = 'hsl(' + (Math.random() * 360) + ',100%,' + (Math.random() * 100) + '%)';
  setTimeout(genLogoPixels, 3000);
};

disableAllInputs = function(state) {
  var allInputs = document.querySelectorAll('button,input');
  for (var i = 0, l = allInputs.length; i < l; i++) allInputs[i].disabled = state;
}

showAnimation = function(animation) { connection.send("S" + animation); };

nextButton.onclick = function() {
  connection.send(">");
  disableAllInputs(true);
  setTimeout(function() { disableAllInputs(false); }, 1000);
}

repeatButton.onclick = function() {
  if (this.classList.contains('active')) connection.send("RF");
  else connection.send("RT");
};

autoBrightness.onchange = function() {
  brightnessDiv.style.display = (!this.checked ? 'inline-block' : 'none');
  connection.send("B" + (this.checked ? "T" : "F") + brightnessRange.value);
};

brightnessRange.onmouseup = function() { connection.send("BF" + brightnessRange.value); };
brightnessRange.ontouchend = function() { onnection.send("BF" + brightnessRange.value); };

sleepMode.onchange = function() {
  if (this.checked) {
    disableAllInputs(true);
    sleepMode.disabled = false;
    connection.send("ZT");
  } else {
    disableAllInputs(false);
    connection.send("ZF");
  }
};

drawAnimTable = function() {
  if (animTableArray.length < 1) return;
  var src = animTableArray.shift();
  connection.send('6/sd/' + src + '/0.bmp');
};

mnuReconnect.onclick = function() { connection.close(); };

var openWebSocket = function() {
  connection = new WebSocket(wsServer);

  wsDisconnected.style.display = 'none';
  wsConnecting.style.display = 'block';
  wsConnected.style.display = 'none';

  connection.onopen = function(evt) {
    console.log("CONNECTED");
    webSocketConnected = true;
    wsDisconnected.style.display = 'none';
    wsConnecting.style.display = 'none';
    wsConnected.style.display = 'block';
    mnuUpdateFirmware.classList.remove('disabled');
    disableAllInputs(false);
  };

  connection.onconnecting = function(evt) {
    wsDisconnected.style.display = 'none';
    wsConnecting.style.display = 'block';
    wsConnected.style.display = 'none';
  };

  connection.onclose = function(evt) {
    console.log("DISCONNECTED");
    webSocketConnected = false;
    wsDisconnected.style.display = 'block';
    wsConnecting.style.display = 'none';
    wsConnected.style.display = 'none';
    curImg.src = "";
    nextImg.src = "";
    repeatButton.classList.remove('active');
    autoBrightness.checked = false;
    sleepMode.checked = false;
    animTableLoaded = false;
    animTable.innerHTML = "";
    animCountPanel.innerHTML = "0 animation(s)";
    ipAddress.innerHTML = "IP: -";
    mnuUpdateFirmware.classList.add('disabled');
    $('.panel-collapse').collapse('hide');
    disableAllInputs(true);

    setTimeout(openWebSocket, 5000);
  };

  connection.onmessage = function(evt) {
    if (evt.data.charAt(0) != "6") console.log(evt.data);

    var msgs = evt.data.split(",");
    for (var i = 0; i < msgs.length; i++) {
      var cmd = msgs[i].charAt(0);

      if (cmd == "C") {
        curImg.src = "http://" + targetLocation + "/sd/" + msgs[i].substring(1) + "/p32.gif";

      } else if (cmd == "N") {
        nextImg.src = "http://" + targetLocation + "/sd/" + msgs[i].substring(1) + "/p32.gif";

      } else if (cmd == "I") {
        ipAddress.innerHTML = "IP: " + msgs[i].substring(1);

      } else if (cmd == "V") {
        pixelFrameVersion = msgs[i].substring(1);
        firmwareVersion.innerHTML = "Firmware: " + pixelFrameVersion;

      } else if (cmd == "B") {
        autoBrightness.checked = (msgs[i].charAt(1) == "T" ? true : false);
        brightnessRange.value = msgs[i].substring(2);
        brightnessDiv.style.display = (!autoBrightness.checked ? 'inline-block' : 'none');

      } else if (cmd == "R") {
        (msgs[i].charAt(1) == "T" ? repeatButton.classList.add('active') : repeatButton.classList.remove('active'));

      } else if (cmd == "Z") {
        sleepMode.checked = (msgs[i].charAt(1) == "T" ? true : false);
        if (sleepMode.checked) {
          disableAllInputs(true);
          sleepMode.disabled = false;
        } else {
          disableAllInputs(false);
        }

      } else if (cmd == "L") {
        if (msgs[i].charCodeAt(1) == 0x04) // EOT
        {
          animCountPanel.innerHTML = animTableArray.length + " animation(s)";
          animTableLoaded = true;
          drawAnimTable();
        } else {
          var anim = msgs[i].substring(1).split("|");
          animTableArray.push(anim[0]);
        }

      } else if (cmd == "6") {
        var response = msgs[i].substring(1).split("|");
        var src = response[0];
        var data = response[1];
        var base = src.split('/')[2]
        
        var img = document.createElement('input');
        img.type = "image";
        img.classList.add('item', 'col-xs-3', 'col-sm-2', 'col-md-2', 'col-lg-1');
        img.onclick = function() { showAnimation(base); };
        img.dataset.src = src;
        img.src = "data:image/bmp;base64," + data;
        if (sleepMode.checked) img.disabled = true;
        animTable.appendChild(img);

        // draw next image
        drawAnimTable();
      }
    }
  };
}

$('.panel-collapse').on('show.bs.collapse', function (e) {
  if (webSocketConnected == true)
  {
    if (animTableLoaded == false) { connection.send("L"); }
    $(this).siblings('.panel-heading').addClass('active');
  }
  else e.preventDefault();
});

$('.panel-collapse').on('hide.bs.collapse', function () {
  $(this).siblings('.panel-heading').removeClass('active');
});

$("#curImg, #nextImg").error(function() {
  var failedUrl = $(this).attr('src');
  if (failedUrl.substring(failedUrl.length - 3, failedUrl.length) == "gif") {
    $(this).attr('src', failedUrl.substr(0, failedUrl.length - 7) + "0.bmp");
  }
});

checkVersion = async function()
{
  var repoTags = await fetch("https://api.bitbucket.org/2.0/repositories/akudaikon/pixel-frame-v2/refs/tags", {cache: "no-store"}).then(response => { return response.json(); });
  repoVersion = repoTags.values[0].target.hash;
  repoVersionCheck = repoVersion.substring(0, 7);

  console.log("Current version: " + pixelFrameVersion + ", Repo version: " + repoVersionCheck);

  if (pixelFrameVersion != undefined && repoVersion != undefined &&
      pixelFrameVersion != repoVersionCheck && !pixelFrameVersion.includes("dirty"))
  {
    document.getElementById("currentVersion").innerHTML = pixelFrameVersion;
    document.getElementById("newVersion").innerHTML = repoVersionCheck;
    document.getElementById("updateChangelog").innerHTML = repoTags.values[0].target.message;
    $('#updateModal').modal('show');
  }
  else $('#noUpdateModal').modal('show');
}

sendUpdate = async function(url, spiffs)
{
  return new Promise(function(resolve, reject)
  {
    connection.send("F" + (spiffs ? "S" : "F") + url);

    connection.onmessage = function(evt)
    {
      if (evt.data == "FOK") resolve();
      else if (evt.data == "FERR") reject();
    }
  });
}

doUpdate = async function()
{
  document.getElementById("updateProgressText").innerHTML = "<i class='fa fa-spinner fa-spin'></i> Update In Progress...";
  document.getElementById("updateProgressSubText").innerHTML = "Do <b>NOT</b> close this page or unplug Pixel Frame.<br>Pixel Frame will automatically restart when complete.";
  $('#updateProgressModal').modal('show');

  try
  {
    await sendUpdate("https://bitbucket.org/akudaikon/pixel-frame-v2/raw/" + repoVersion + "/main/spiffs.bin", true);
  }
  catch
  {
    document.getElementById("updateProgressText").innerHTML = "<i class='fas fa-exclamation-triangle' style='color:firebrick'></i> Update Failed!";
    document.getElementById("updateProgressSubText").innerHTML = "Pixel Frame will now restart. Please refresh this page and try again.";
    connection.send("!");
  }

  try
  {
    await sendUpdate("https://bitbucket.org/akudaikon/pixel-frame-v2/raw/" + repoVersion + "/main/firmware.bin", false);
  }
  catch
  {
    document.getElementById("updateProgressText").innerHTML = "<i class='fas fa-exclamation-triangle' style='color:firebrick'></i> Update Failed!";
    document.getElementById("updateProgressSubText").innerHTML = "Pixel Frame will now restart. Please refresh this page and try again.";
    connection.send("!");
  }

  document.getElementById("updateProgressText").innerHTML = "Update Complete! <i class='fas fa-check' style='color:green'></i>";
  document.getElementById("updateProgressSubText").innerHTML = "Pixel Frame will now restart. Please refresh this page.";
  connection.send("!");
}

mnuUpdateFirmware.onclick = function() { if (webSocketConnected) checkVersion(); }
updateButton.onclick = function() { doUpdate(); }
forceUpdateButton.onclick = function() { doUpdate(); }

genLogoPixels();
openWebSocket();