#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "lwip/api.h"
#include "lwip/err.h"
#include "lwip/netdb.h"
#include "esp_ota_ops.h"

#include "eventgroup_defs.h"
#include "websocket_server.h"
#include "director.h"
#include "settings.h"
#include "base64.h"
#include "util.h"

static void sendFile(struct netconn *conn, char *fname)
{
  char buffer[512];
  size_t bytesRead = 0;

  // root should point to SPIFFS, so tack it on
  if ((strrchr(fname, '/') - fname) == 0)
  {
    strcpy(buffer, "/spiffs");
    strcat(buffer, fname);
  }
  else strcpy(buffer, fname);

  // if fname is just a directory, tack on index.htm
  if ((strrchr(fname, '/') - fname) == strlen(fname) - 1) strcat(buffer, "index.htm");

  // try to open requested file
  FILE *file = fopen(buffer, "rb");
  if (file != NULL)
  {
    char ext[4];
    strncpy(ext, &buffer[strlen(buffer) - 3], strlen(buffer)); // get file's extension

    // send headers
    sprintf(buffer, "HTTP/1.1 200 OK\r\n");
    netconn_write(conn, buffer, strlen(buffer), NETCONN_COPY);

    if (!strcmp(ext, "htm")) sprintf(buffer, "Content-Type: text/html\r\n");
    else if (!strcmp(ext, "css")) sprintf(buffer, "Content-Type: text/css\r\n");
    else if (!strcmp(ext, ".js")) sprintf(buffer, "Content-Type: text/javascript\r\n");
    else if (!strcmp(ext, "bmp")) sprintf(buffer, "Content-Type: image/bmp\r\n");
    else if (!strcmp(ext, "gif")) sprintf(buffer, "Content-Type: image/gif\r\n");
    else sprintf(buffer, "Content-Type: text/plain\r\n");
    netconn_write(conn, buffer, strlen(buffer), NETCONN_COPY);

    fseek(file, 0, SEEK_END);
    sprintf(buffer, "Content-Length: %d\r\n", (int)ftell(file));
    netconn_write(conn, buffer, strlen(buffer), NETCONN_COPY);
    rewind(file);

    sprintf(buffer, "X-Powered-By: RainbowsAndUnicorns\r\n\r\n");
    netconn_write(conn, buffer, strlen(buffer), NETCONN_COPY);

    // stream file
    bzero(buffer, sizeof(buffer));
    while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0)
    {
      netconn_write(conn, buffer, sizeof(buffer), NETCONN_NOCOPY);
    }
    fclose(file);
  }
  else
  {
    // if file doesn't exist, send 404
    sprintf(buffer, "HTTP/1.1 404 Not Found\r\n\r\n");
    netconn_write(conn, buffer, strlen(buffer), NETCONN_COPY);
  }
}

static void updateFirmware(struct netconn *conn)
{
  /*
  esp_ota_handle_t update_handle = 0;
  const esp_partition_t *update_partition = NULL;

  ESP_LOGI(__func__, "Starting Firmware OTA...");

  update_partition = esp_ota_get_next_update_partition(NULL);
  if (update_partition == NULL)
  {
    ESP_LOGE(__func__, "Firmware OTA partition not found");
    const char response[] = "HTTP/1.1 500 Internal Server Error\r\nContent-Type: text/html\r\n\r\nFirmware OTA partition not found";
    netconn_write(conn, response, strlen(response), NETCONN_NOCOPY);
    goto firmware_fail;
  }

  ESP_LOGI(__func__, "Writing to partition subtype %d at offset 0x%x", update_partition->subtype, update_partition->address);

  esp_err_t err = esp_ota_begin(update_partition, OTA_SIZE_UNKNOWN, &update_handle);
  if (err != ESP_OK)
  {
    ESP_LOGE(__func__, "esp_ota_begin failed, error=%d", err);
    const char response[] = "HTTP/1.1 500 Internal Server Error\r\nContent-Type: text/html\r\n\r\nesp_ota_begin failed";
    netconn_write(conn, response, strlen(response), NETCONN_NOCOPY);
    goto firmware_fail;
  }

  ESP_LOGI(__func__, "esp_ota_begin succeeded");

  esp_err_t ota_write_err = ESP_OK;
  while (1)
  {
    char chunk[1024] = { 0 };
    int recv_len = httpd_req_recv(req, chunk, 1023);
    if (recv_len < 0) { ota_write_err = ESP_FAIL; break; }
    if (recv_len == 0) break;

    ota_write_err = esp_ota_write(update_handle, (const void *)chunk, recv_len);
    if (ota_write_err != ESP_OK) break;
  }

  esp_err_t ota_end_err = esp_ota_end(update_handle);
  if (ota_write_err != ESP_OK)
  {
    ESP_LOGE(__func__, "Error: esp_ota_write failed! err=0x%d", err);
    const char response[] = "HTTP/1.1 500 Internal Server Error\r\nContent-Type: text/html\r\n\r\nesp_ota_write failed!";
    netconn_write(conn, response, strlen(response), NETCONN_NOCOPY);

    goto firmware_fail;
  }
  else if (ota_end_err != ESP_OK)
  {
    ESP_LOGE(__func__, "Error: esp_ota_end failed! err=0x%d. Image is invalid", ota_end_err);
    const char response[] = "HTTP/1.1 500 Internal Server Error\r\nContent-Type: text/html\r\n\r\nesp_ota_end failed! Image is invalid";
    netconn_write(conn, response, strlen(response), NETCONN_NOCOPY);
    goto firmware_fail;
  }

  if (esp_ota_set_boot_partition(update_partition) != ESP_OK)
  {
    ESP_LOGE(__func__, "esp_ota_set_boot_partition failed! err=0x%d", err);
    const char response[] = "HTTP/1.1 500 Internal Server Error\r\nContent-Type: text/html\r\n\r\nesp_ota_set_boot_partition failed!";
    netconn_write(conn, response, strlen(response), NETCONN_NOCOPY);
    goto firmware_fail;
  }

  ESP_LOGI(__func__, "Firmware OTA successful!");
  const char response[] = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\nUpdate successful! Restarting...";
  netconn_write(conn, response, strlen(response), NETCONN_NOCOPY);

  firmware_fail:
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    esp_restart();
  */
}

static void updateSPIFFS(struct netconn *conn)
{
  /*
  const esp_partition_t *update_partition = NULL;

  ESP_LOGI(__func__, "Starting SPIFFS OTA...");

  update_partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_SPIFFS, NULL);
  if (update_partition == NULL)
  {
    ESP_LOGE(__func__, "SPIFFS partition not found");
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "SPIFFS partition not found");
    goto spiffs_fail;
  }
  ESP_LOGI(__func__, "Writing to partition subtype %d at offset 0x%x", update_partition->subtype, update_partition->address);

  ESP_LOGI(__func__, "Erasing SPIFFS partition...");
  esp_err_t err = esp_partition_erase_range(update_partition, 0, update_partition->size);
  if (err != ESP_OK)
  {
    ESP_LOGE(__func__, "Error erasing SPIFFS partition, error=%d", err);
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Error erasing SPIFFS partition");
    goto spiffs_fail;
  }
  ESP_LOGI(__func__, "SPIFFS partition erase successful");

  // Start writing SPIFFS OTA data to partition
  ESP_LOGI(__func__, "Writing data to SPIFFS partition...");
  esp_err_t write_err = ESP_OK;
  size_t data_offset = 0;
  while (1)
  {
    char chunk[1024] = { 0 };
    int recv_len = httpd_req_recv(req, chunk, 1024);
    if (recv_len < 0) { write_err = ESP_ERR_INVALID_RESPONSE; break; }
    if (recv_len == 0) break;

    write_err = esp_partition_write(update_partition, data_offset, (const void *)chunk, recv_len);
    if (write_err != ESP_OK) break;
    data_offset += recv_len;
  }

  if (write_err != ESP_OK)
  {
    ESP_LOGE(__func__, "Error: esp_partition_write failed! err=0x%d", write_err);
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "esp_partition_write failed!");
    goto spiffs_fail;
  }

  ESP_LOGI(__func__, "SPIFFS OTA successful!");
  httpd_resp_set_status(req, HTTPD_200);
  httpd_resp_send(req, "SPIFFS update successful!", 25);

  spiffs_fail:
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    esp_restart();
  */
}

void websocket_callback(uint8_t num, WEBSOCKET_TYPE_t type, char* msg, uint64_t len)
{
  switch (type)
  {
    case WEBSOCKET_CONNECT:
    {
      ESP_LOGI(__func__, "client %i connected!", num);
      director_sendUpdate(UPDATE_HELLO | UPDATE_IP_VERSION | UPDATE_BRIGHT_MODES | UPDATE_CUR_NEXT_ANIM);
      break;
    }

    case WEBSOCKET_DISCONNECT_EXTERNAL:
      ESP_LOGI(__func__, "client %i sent a disconnect", num);
      break;

    case WEBSOCKET_DISCONNECT_INTERNAL:
      ESP_LOGI(__func__, "client %i was disconnected", num);
      break;

    case WEBSOCKET_DISCONNECT_ERROR:
      ESP_LOGI(__func__, "client %i was disconnected due to an error", num);
      break;

    case WEBSOCKET_TEXT:
      if (len)
      {
        ESP_LOGI(__func__, "Received: %s", msg);
        switch (msg[0])
        {
          // set brightness
          case 'B':
          {
            if (msg[1] == 'T')
            {
              settings.autoBrightness = true;
              director_sendUpdate(UPDATE_BRIGHT_MODES);
            }
            else
            {
              settings.autoBrightness = false;
              director_setBrightness(atoi((char*)&msg[2]));
            }
          }
          break;

          // send list of animations
          case 'L':
          {
            FILE *animationList = fopen("/sd/system/folders.lst", "r");

            if (animationList != NULL)
            {
              char line[261];  // 'L' + 255 max path name + '|' + 3 digit version + newline
              line[0] = 'L';

              while (fgets(line + 1, 259, animationList) != NULL)
              {
                ws_server_send_text_client_from_callback(num, line, strlen(line));
              }
              fclose(animationList);

              // L + EOT character to indicate end of animation list
              line[1] = 0x04; // EOT character
              ws_server_send_text_client_from_callback(num, line, 2);
            }
          }
          break;

          // play next
          case '>':
          {
            director_playNext("");
          }
          break;

          // repeat mode
          case 'R':
          {
            if (msg[1] == 'T') director_setRepeat(true);
            else director_setRepeat(false);
          }
          break;

          // sleep mode
          case 'Z':
          {
            if (msg[1] == 'T') director_sleep(true);
            else director_sleep(false);
          }
          break;

          // show specific animation
          case 'S':
          {
            director_playNext(&msg[1]);
          }
          break;

          // send image over websocket
          case '6':
          {
            FILE *file = fopen((char*)&msg[1], "rb");
            if (file != NULL)
            {
              fseek(file, 0, SEEK_END);
              size_t bmpSize = ftell(file);
              rewind(file);

              size_t b64len = ((4 * bmpSize / 3) + 3) & ~3;
              size_t offset = strlen((char*)&msg[1]) + 2;
              b64len += offset;

              char retData[b64len];
              strcpy(retData, "6");
              strcat(retData, (char*)&msg[1]);
              strcat(retData, "|");
              base64_encode_file(file, retData + offset, b64len);
              ws_server_send_text_client_from_callback(num, retData, b64len);
              fclose(file);
            }
          }
          break;

          // download file to SD
          case 'D':
          {
            if (director_downloadFile((char*)&msg[1]) == ESP_OK) ws_server_send_text_client_from_callback(num, "DOK", 3);
            else ws_server_send_text_client_from_callback(num, "DERR", 4);
          }
          break;

          // delete animation from SD
          case 'X':
          {
            if (director_deleteAnim((char*)&msg[1]) == ESP_OK) ws_server_send_text_client_from_callback(num, "XOK", 3);
            else ws_server_send_text_client_from_callback(num, "XERR", 4);
          }
          break;

          // reindex animations and start directing
          case '+':
          {
            director_reindexAnims();
            ws_server_send_text_client_from_callback(num, "+OK", 3);
          }
          break;

          // firmware/SPIFFS update
          case 'F':
          {
            if (msg[1] == 'S') // SPIFFS
            {
              if (director_updateSPIFFS((char*)&msg[2]) == ESP_OK) ws_server_send_text_client_from_callback(num, "FOK", 3);
              else ws_server_send_text_client_from_callback(num, "FERR", 4);
            }
            else if (msg[1] == 'F') // firmware
            {
              if (director_updateFirmware((char*)&msg[2]) == ESP_OK) ws_server_send_text_client_from_callback(num, "FOK", 3);
              else ws_server_send_text_client_from_callback(num, "FERR", 4);
            }
            else ws_server_send_text_client_from_callback(num, "FERR", 4);
          }
          break;

          // restart Pixel Frame
          case '!':
          {
            director_restart();
          }
          break;
        }
      }
      break;

    default:
      break;
  }
}

void sendWebSocketUpdate(char* curAnim, char* nextAnim, uint8_t updateBits)
{
  if (!(xEventGroupGetBits(xEventGroup) & WIFI_CONNECTED)) return;

  char *update;

  if (updateBits & UPDATE_HELLO)
  {
    update = calloc(19, sizeof(char));
    strcpy(update, "Hi! <3 Pixel Frame");
    ws_server_send_text_all(update, strlen(update));
    free(update);
  }

  if (updateBits & UPDATE_IP_VERSION)
  {
    update = calloc(1 + strlen(settings.ipAddress) + 2 + strlen(getFirmwareVersion()) + 1, sizeof(char));
    strcpy(update, "I");
    strcat(update, settings.ipAddress);
    strcat(update, ",V");
    strcat(update, getFirmwareVersion());
    ESP_LOGI(__func__, "Sending websocket update: %s", update);
    ws_server_send_text_all(update, strlen(update));
    free(update);
  }

  if (updateBits & UPDATE_BRIGHT_MODES)
  {
    char brightnessChar[4];
    itoa(settings.brightness, brightnessChar, 10);
    update = calloc(8 + strlen(brightnessChar) + 1, sizeof(char));
    strcpy(update, "B");
    strcat(update, (settings.autoBrightness ? "T" : "F"));
    strcat(update, brightnessChar);
    strcat(update, ",R");
    strcat(update, (settings.repeatMode ? "T": "F"));
    strcat(update, ",Z");
    strcat(update, (settings.sleepMode ? "T": "F"));
    ESP_LOGI(__func__, "Sending websocket update: %s", update);
    ws_server_send_text_all(update, strlen(update));
    free(update);
  }

  if (updateBits & UPDATE_CUR_NEXT_ANIM)
  {
    update = calloc(1 + strlen(curAnim) + 1, sizeof(char));
    strcpy(update, "C");
    strcat(update, curAnim);
    ESP_LOGI(__func__, "Sending websocket update: %s", update);
    ws_server_send_text_all(update, strlen(update));
    free(update);

    update = calloc(1 + strlen(nextAnim) + 1, sizeof(char));
    strcpy(update, "N");
    strcat(update, nextAnim);
    ESP_LOGI(__func__, "Sending websocket update: %s", update);
    ws_server_send_text_all(update, strlen(update));
    free(update);
  }
}

static void http_server_netconn_serve(struct netconn *conn)
{
  struct netbuf *inbuf;
  char *buf;
  uint16_t buflen;
  err_t err;

  /* Read the data from the port, blocking if nothing yet there.
   We assume the request (the part we care about) is in one netbuf */
  err = netconn_recv(conn, &inbuf);

  if (err == ERR_OK)
  {
    netbuf_data(inbuf, (void**)&buf, &buflen);

    // Handle HTTP request appropriately
    if (buflen >= 4 &&
        buf[0] == 'G' &&
        buf[1] == 'E' &&
        buf[2] == 'T' &&
        buf[3] == ' ')
    {
      char url[256];
      if (sscanf(buf, "GET %255s HTTP/1.1\n", url) != 1) goto close_conn;
      ESP_LOGI(__func__, "HTTP GET: %s", url);

      // Do appropriate action depending on what client is requesting
      if (!strcmp(url, "/websocket") && strstr(buf, "Upgrade: websocket"))
      {
        ws_server_add_client(conn, buf, buflen, "/websocket", websocket_callback);
        goto dont_close_conn;
      }
      else if (!strcmp(url, "/next"))
      {
        director_playNext("");
        const char response[] = "HTTP/1.1 200 OK\r\n\r\n";
        netconn_write(conn, response, strlen(response), NETCONN_NOCOPY);
      }
      else if (!strcmp(url, "/sleep"))
      {
        director_sleep(true);
        const char response[] = "HTTP/1.1 200 OK\r\n\r\n";
        netconn_write(conn, response, strlen(response), NETCONN_NOCOPY);
      }
      else if (!strcmp(url, "/wake"))
      {
        director_sleep(false);
        const char response[] = "HTTP/1.1 200 OK\r\n\r\n";
        netconn_write(conn, response, strlen(response), NETCONN_NOCOPY);
      }
      else if (!strcmp(url, "/restart"))
      {
        const char response[] = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\nRestarting...";
        netconn_write(conn, response, strlen(response), NETCONN_NOCOPY);
        netconn_close(conn);
        director_restart();
      }
      else if (!strcmp(url, "/update"))
      {
        // update.htm is embedded in program code (in component.mk)
        extern const unsigned char update_htm_start[] asm("_binary_update_htm_start");
        extern const unsigned char update_htm_end[]   asm("_binary_update_htm_end");
        const size_t update_htm_size = (update_htm_end - update_htm_start);

        const char response[] = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";
        netconn_write(conn, response, strlen(response), NETCONN_NOCOPY);
        netconn_write(conn, update_htm_start, update_htm_size, NETCONN_NOCOPY);
      }
      else sendFile(conn, url);
    }
    else if (buflen >= 5 &&
             buf[0] == 'P' &&
             buf[1] == 'O' &&
             buf[2] == 'S' &&
             buf[3] == 'T' &&
             buf[4] == ' ')
    {
      char url[128];
      if (sscanf(buf, "POST %127s HTTP/1.1\n", url) != 1) goto close_conn;
      ESP_LOGI(__func__, "HTTP POST: %s", url);

      if (!strcmp(url, "/firmware")) updateFirmware(conn);
      else if (!strcmp(url, "/spiffs")) updateSPIFFS(conn);
      else
      {
        const char response[] = "HTTP/1.1 404 Not Found\r\n\r\n";
        netconn_write(conn, response, strlen(response), NETCONN_NOCOPY);
      }
    }
  }

  close_conn:
  // Close the connection (server closes in HTTP)
  netconn_close(conn);
  netconn_delete(conn);

  dont_close_conn:
  // Delete the buffer
  netbuf_delete(inbuf);
}

void httpServer_task(void *pvParameters)
{
  xEventGroupWaitBits(xEventGroup, (WIFI_CONNECTED | SDCARD_MOUNTED | SPIFFS_MOUNTED), pdFALSE, pdTRUE, portMAX_DELAY);

  struct netconn *conn, *newconn;
  err_t err;

  conn = netconn_new(NETCONN_TCP);
  netconn_bind(conn, NULL, 80);
  netconn_listen(conn);

  ESP_LOGI(__func__, "HTTP server task started on core %d", xPortGetCoreID());
  xEventGroupSetBits(xEventGroup, HTTP_TASK_STARTED);

  ws_server_start();
  xEventGroupSetBits(xEventGroup, WEBSOCKETS_TASK_STARTED);

  do
  {
    err = netconn_accept(conn, &newconn);
    if (err == ERR_OK)
    {
      http_server_netconn_serve(newconn);
    }
  } while (err == ERR_OK);

  netconn_close(conn);
  netconn_delete(conn);

  ESP_LOGI(__func__, "HTTP server task stopped");
  vTaskDelete(NULL);
}