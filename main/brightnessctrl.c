#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "esp_log.h"

#include "pins.h"
#include "eventgroup_defs.h"
#include "director.h"
#include "settings.h"
#include "brightnessctrl.h"

static bool taskStop = false;
static uint32_t adcSamples = 0;
static uint8_t numADCSamples = 0;

void brightnessCtrl_task(void *pvParameters)
{
  adc1_config_width(ADC_WIDTH_BIT_12);
  adc1_config_channel_atten(LDR_PIN, ADC_ATTEN_DB_11);

  ESP_LOGI(__func__, "Brightness control task started on core %d", xPortGetCoreID());

  while (!taskStop)
  {
    if (numADCSamples < ADC_SAMPLES_PER_UPDATE)
    {
      adcSamples =+ adc1_get_raw(LDR_PIN);
      numADCSamples++;
    }
    else
    {
      adcSamples /= ADC_SAMPLES_PER_UPDATE;
      if (adcSamples < LDR_MIN) adcSamples = LDR_MIN;
      else if (adcSamples > LDR_MAX) adcSamples = LDR_MAX;

      uint16_t newBrightness = (adcSamples - LDR_MIN) * (BRIGHTNESS_MAX - BRIGHTNESS_MIN) / (LDR_MAX - LDR_MIN) + BRIGHTNESS_MIN;
      //ESP_LOGI(__func__, "%d %d", adcSamples, newBrightness);

      if (settings.brightness != newBrightness && settings.autoBrightness) director_setBrightness(newBrightness);

      xEventGroupSetBits(xEventGroup, BRIGHTNESS_TASK_STARTED);
      adcSamples = 0;
      numADCSamples = 0;
    }

    vTaskDelay(ADC_SAMPLE_RATE_MS / portTICK_PERIOD_MS);
  }

  vTaskDelete(NULL);
}

void brightnessCtrl_stop()
{
  taskStop = true;
  xEventGroupClearBits(xEventGroup, BRIGHTNESS_TASK_STARTED);
}
