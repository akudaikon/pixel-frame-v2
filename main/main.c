#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "mdns.h"
#include "esp_err.h"
#include "esp_log.h"
#include "sdkconfig.h"

#include "eventgroup_defs.h"
#include "pins.h"
#include "touch.h"
#include "settings.h"
#include "ledmatrix.h"
#include "director.h"
#include "wifi.h"
#include "mqtt.h"
#include "httpserver.h"
#include "brightnessctrl.h"
#include "util.h"

settingsData_t settings;
EventGroupHandle_t xEventGroup = NULL;

void app_main()
{
  xEventGroup = xEventGroupCreate();
  settings.brightness = BRIGHTNESS_MAX;

  // animation related tasks run on core 1
  // wifi (and all other tasks) run on core 0

  // start the LED matrix driver task on core 1
  xTaskCreatePinnedToCore(&ledMatrix_task, "ledMatrix", 4096, NULL, 1, NULL, 1);
  xEventGroupWaitBits(xEventGroup, LEDMATRIX_TASK_STARTED, pdFALSE, pdTRUE, portMAX_DELAY);

  // initialize and mount SPIFFS
  if (initSPIFFS() == ESP_FAIL)
  {
    drawSPIFFSErr();
    return;
  }
  xEventGroupWaitBits(xEventGroup, SPIFFS_MOUNTED, pdFALSE, pdTRUE, portMAX_DELAY);

  // initialize and mount SD card
  if (initSD() == ESP_FAIL)
  {
    drawBitmap("/spiffs/sderr.bmp");
    return;
  }
  xEventGroupWaitBits(xEventGroup, SDCARD_MOUNTED, pdFALSE, pdTRUE, portMAX_DELAY);

  // read settings from SD system/config.ini
  if (readSettings() == ESP_FAIL)
  {
    drawBitmap("/spiffs/inierr.bmp");
    return;
  }

  // start animation director task on core 1
  xTaskCreatePinnedToCore(&director_task, "director", 8192, NULL, 2, NULL, 1);

  // start the brightness control task on core 0
  xTaskCreatePinnedToCore(&brightnessCtrl_task, "brightness", 2048, NULL, 2, NULL, 0);

  xEventGroupWaitBits(xEventGroup, (DIRECTOR_TASK_STARTED | BRIGHTNESS_TASK_STARTED), pdFALSE, pdTRUE, portMAX_DELAY);
  vTaskDelay(2000 / portTICK_PERIOD_MS);

  // connect to wifi
  wifiConnect(settings.wifiSSID, settings.wifiPassword);

  // wait for wifi to connect and show animation
  uint8_t animFrame = 0;
  TickType_t startTick = xTaskGetTickCount();
  while (1)
  {
    char bmpFile[18];
    char frameChar[2];
    itoa(animFrame, frameChar, 10);
    strcpy(bmpFile, "/spiffs/wifi");
    strcat(bmpFile, frameChar);
    strcat(bmpFile, ".bmp");
    drawBitmap(bmpFile);
    if (++animFrame > 2) animFrame = 0;

    if (xEventGroupGetBits(xEventGroup) & WIFI_CONNECTED)
    {
      drawBitmap("/spiffs/wifiok.bmp");
      break;
    }
    else if (xTaskGetTickCount() - startTick > pdMS_TO_TICKS(WIFI_CONNECT_TIMEOUT) || xEventGroupGetBits(xEventGroup) & WIFI_DISCONNECTED)
    {
      wifiDisconnect();
      drawBitmap("/spiffs/wifierr.bmp");
      break;
    }
    vTaskDelay(500 / portTICK_PERIOD_MS);
  };
  vTaskDelay(3000 / portTICK_PERIOD_MS);

  // start HTTP/websocket server on core 0
  xTaskCreatePinnedToCore(&httpServer_task, "httpServer", 8192, NULL, 1, NULL, 0);

  // start mdns server
  if (mdns_init() == ESP_OK)
  {
    mdns_hostname_set("pixelframe");
    mdns_instance_name_set("Pixel Frame 32X");
    ESP_LOGI(__func__, "MDNS server started");
  }

  // start MQTT task (if enabled)
  if (settings.mqttEnable) startMQTT(settings.mqttURI);

  // start the touch task on core 0
  //xTaskCreatePinnedToCore(&touch_task, "touch", 2048, NULL, 3, NULL, 0);

  // show intro animation and start normal operation
  director_playNext("system/intro");

  // while(1)
  // {
  //   touch_processTouch();
  //   vTaskDelay(100 / portTICK_PERIOD_MS);
  // }

  return;
}