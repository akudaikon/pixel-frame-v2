#ifndef MQTT_H
#define MQTT_H

esp_err_t startMQTT(char* uri);
void sendMQTTUpdate(char* curAnim, char* nextAnim);

#endif