#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_wifi.h"

#include "cJSON.h"
#include "eventgroup_defs.h"
#include "director.h"
#include "settings.h"
#include "mqtt_client.h"

static esp_mqtt_client_handle_t mqttClient;

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
  esp_mqtt_client_handle_t client = event->client;

  switch (event->event_id) 
  {
    case MQTT_EVENT_CONNECTED:
      xEventGroupSetBits(xEventGroup, MQTT_CONNECTED);
      esp_mqtt_client_subscribe(client, "pixelframe/command", 0);
      director_sendUpdate(0);
      break;

    case MQTT_EVENT_DISCONNECTED:
      xEventGroupClearBits(xEventGroup, MQTT_CONNECTED);
      break;

    case MQTT_EVENT_DATA:
      ESP_LOGI(__func__, "%.*s: %.*s", event->topic_len, event->topic, event->data_len, event->data);

      if (!strncmp(event->topic, "pixelframe/command", event->topic_len))
      {
        cJSON *root = cJSON_Parse(event->data);

        if (root != NULL)
        {
          const cJSON *item = NULL;

          if (cJSON_HasObjectItem(root, "state"))
          {
            item = cJSON_GetObjectItemCaseSensitive(root, "state");
            if (!strcasecmp(item->valuestring, "on")) director_sleep(false);
            else if (!strcasecmp(item->valuestring, "off")) director_sleep(true);
          }

          if (cJSON_HasObjectItem(root, "brightness"))
          {
            item = cJSON_GetObjectItemCaseSensitive(root, "brightness");
            if (cJSON_IsNumber(item))
            {
              settings.autoBrightness = false;
              director_setBrightness(item->valueint);
            }
          }

          if (cJSON_HasObjectItem(root, "play"))
          {
            item = cJSON_GetObjectItemCaseSensitive(root, "play");
            director_playNext(item->valuestring);
          }

          if (cJSON_HasObjectItem(root, "next"))
          {
            director_playNext(0);
          }

          if (cJSON_HasObjectItem(root, "repeat"))
          {
            item = cJSON_GetObjectItemCaseSensitive(root, "repeat");
            if (cJSON_IsBool(item))
            {
              if (cJSON_IsTrue(item)) director_setRepeat(true);
              else director_setRepeat(false);
            }
          }
        }
        else ESP_LOGE(__func__, "Error parsing JSON from MQTT message");
      }
      break;

    case MQTT_EVENT_ERROR:
      ESP_LOGI(__func__, "MQTT_EVENT_ERROR");
      break;

    default:
      break;
  }
  return ESP_OK;
}

void sendMQTTUpdate(char* curAnim, char* nextAnim)
{
  if (!(xEventGroupGetBits(xEventGroup) & WIFI_CONNECTED)) return;
  if (!(xEventGroupGetBits(xEventGroup) & MQTT_CONNECTED)) return;

  cJSON *root = NULL;
  root = cJSON_CreateObject();
  cJSON_AddStringToObject(root, "state", (settings.sleepMode ? "OFF" : "ON"));
  cJSON_AddStringToObject(root, "current", curAnim);
  cJSON_AddStringToObject(root, "next", nextAnim);
  cJSON_AddNumberToObject(root, "brightness", settings.brightness);
  cJSON_AddBoolToObject(root, "repeat", settings.repeatMode);
  char *mqttUpdate = cJSON_PrintUnformatted(root);

  ESP_LOGI(__func__, "Sending MQTT update: %s", mqttUpdate);
  esp_mqtt_client_publish(mqttClient, "pixelframe/state", mqttUpdate, 0, 0, 0);
  cJSON_Delete(root);
  free(mqttUpdate);
}

esp_err_t startMQTT(char* uri)
{
  xEventGroupWaitBits(xEventGroup, WIFI_CONNECTED, pdFALSE, pdTRUE, portMAX_DELAY);

  esp_mqtt_client_config_t mqtt_cfg = 
  {
    .event_handle = mqtt_event_handler,
    .lwt_topic = "pixelframe/state",
    .lwt_msg = "{\"state\":\"OFF\"}",
    .lwt_msg_len = 15,
    .uri = uri,
  };

  ESP_LOGI(__func__, "Connecting to MQTT: %s", mqtt_cfg.uri);
  mqttClient = esp_mqtt_client_init(&mqtt_cfg);
  esp_mqtt_client_start(mqttClient);
  xEventGroupSetBits(xEventGroup, MQTT_TASK_STARTED);

  return ESP_OK;
}