#ifndef LEDMATRIX_H
#define LEDMATRIX_H

#include "freertos/queue.h"

#define MATRIX_HEIGHT             32
#define MATRIX_WIDTH              32
#define MATRIX_HEIGHTDIV2         (MATRIX_HEIGHT / 2)

#define NUM_BITPLANES             7
#define NUM_FRAME_BUFFERS         2
#define OE_OFF_CLKS_AFTER_LATCH   1
#define LSBMSB_TRANSITION_BIT     1

#define I2S_BIT_R1                (1 << 0)
#define I2S_BIT_G1                (1 << 1)
#define I2S_BIT_B1                (1 << 2)
#define I2S_BIT_R2                (1 << 3)
#define I2S_BIT_G2                (1 << 4)
#define I2S_BIT_B2                (1 << 5)

#define I2S_BIT_A                 (1 << 8)
#define I2S_BIT_B                 (1 << 9)
#define I2S_BIT_C                 (1 << 10)
#define I2S_BIT_D                 (1 << 11)
#define I2S_BIT_LAT               (1 << 12)
#define I2S_BIT_OE                (1 << 13)

typedef struct rowBitStruct_t
{
  uint16_t data[MATRIX_WIDTH];
} rowBitStruct_t;

typedef struct rowDataStruct_t
{
  rowBitStruct_t rowbits[NUM_BITPLANES];
} rowDataStruct_t;

typedef struct frameStruct_t
{
  rowDataStruct_t rowdata[MATRIX_HEIGHTDIV2];
} frameStruct_t;

typedef union
{
  union
  {
    uint32_t color;
    struct
    {
      uint8_t red;
      uint8_t green;
      uint8_t blue;
      uint8_t unused;
    };
  };
} pixelColor_t;

QueueHandle_t ledMatrixQueue;

void ledMatrix_task(void *pvParameters);

#endif