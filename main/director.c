#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_timer.h"

#include "eventgroup_defs.h"
#include "ledmatrix.h"
#include "gamma.h"
#include "director.h"
#include "ini.h"
#include "settings.h"
#include "mqtt.h"
#include "httpserver.h"
#include "util.h"
#include "brightnessctrl.h"

static uint16_t numAnimations = 0;
static animationState_t curAnimation;
static animationState_t nextAnimation;

static pixelColor_t pixelData[MATRIX_HEIGHT][MATRIX_WIDTH] = { 0 };
pixelColor_t *pixelDataPtr = *pixelData;

static esp_timer_handle_t frameTimer, animationTimer;

static void getNextFrame();
static void getNextAnimation(bool show);
static void indexAnimations();

static void readAnimConfig(animationState_t *animState)
{
  char *iniFile = calloc(4 + strlen(animState->path) + 11 + 1, sizeof(char));
  strcpy(iniFile, "/sd/");
  strcat(iniFile, animState->path);
  strcat(iniFile, "/config.ini");

  ini_t *config = ini_load(iniFile);

  if (config != NULL)
  {
    if (ini_sget(config, "animation", "fps", "%d", &(animState->frameTime)) == 0)
    {
      // backwards compatability for "frame time"
      if (ini_sget(config, "animation", "frame time", "%d", &(animState->frameTime)) == 0) animState->frameTime = settings.defaultFrameTime;
    }
    else animState->frameTime = (1000 / animState->frameTime);

    if (ini_sget(config, "animation", "animation time", "%d", &(animState->animTime)) == 0) animState->animTime = settings.defaultAnimTime;
    if (ini_bget(config, "animation", "loop", &(animState->loop)) == 0) animState->loop = settings.defaultLoop;
    ini_free(config);

    // offset to account for overhead between each frame
    animState->frameTime = (animState->frameTime > (FRAME_TIME_OFFSET + MIN_FRAME_TIME) ? animState->frameTime - FRAME_TIME_OFFSET : MIN_FRAME_TIME);
  }
  else
  {
    animState->frameTime = settings.defaultFrameTime;
    animState->animTime = settings.defaultAnimTime;
    animState->loop = settings.defaultLoop;
  }
  free(iniFile);
}

static void getNextFrame()
{
  esp_timer_stop(frameTimer);

  if (settings.sleepMode) return;

  // compile frame path and filename
  char frameChar[6];
  itoa(curAnimation.curFrame, frameChar, 10);
  char* bmpFile = calloc(4 + strlen(curAnimation.path) + 1 + strlen(frameChar) + 4 + 1, sizeof(char));
  strcpy(bmpFile, "/sd/");
  strcat(bmpFile, curAnimation.path);
  strcat(bmpFile, "/");
  strcat(bmpFile, frameChar);
  strcat(bmpFile, ".bmp");

  // if file doesn't exist, loop back to first frame or start next animation
  struct stat fileStat;
  if (stat(bmpFile, &fileStat) < 0)
  {
    if (!curAnimation.loop && !settings.repeatMode) { getNextAnimation(true); return; }
    curAnimation.curFrame = 0;
    strcpy(bmpFile, "/sd/");
    strcat(bmpFile, curAnimation.path);
    strcat(bmpFile, "/0.bmp");
  }

  // display frame
  drawBitmap(bmpFile);
  free(bmpFile);
  curAnimation.curFrame++;
  esp_timer_start_once(frameTimer, curAnimation.frameTime * 1000);
}

static void getNextAnimation(bool show)
{
  esp_timer_stop(frameTimer);
  esp_timer_stop(animationTimer);

  if (settings.sleepMode) return;

  // set current animation to next animation
  curAnimation = nextAnimation;

  // start looking for next animation
  DIR *dirPtr = opendir("/sd/");
  struct dirent* entry;
  struct stat fileStat;
  bool alreadyLooped = false;
  uint16_t randomCounter = (esp_random() % numAnimations) + 1;

  while (1)
  {
    entry = readdir(dirPtr);
    if (!entry)
    {
      if (alreadyLooped) 
      {
        drawBitmap("/spiffs/noanim.bmp");
        while(1);
      }
      alreadyLooped = true;
      rewinddir(dirPtr);
      continue;
    }

    // if we found a folder, see if it's the one we want
    if (entry->d_type == DT_DIR && entry->d_name[0] != '.')
    {
      // if random order, keep skipping until randomCounter is zero
      if (--randomCounter > 0) continue;

      // check for 0.bmp file in folder, else keep looking
      char* bmpFile = calloc(4 + strlen(entry->d_name) + 6 + 1, sizeof(char));
      strcpy(bmpFile, "/sd/");
      strcat(bmpFile, entry->d_name);
      strcat(bmpFile, "/0.bmp");

      if (stat(bmpFile, &fileStat) == 0)
      {
        // folder has a 0.bmp file, so this is the one!
        strcpy(nextAnimation.path, entry->d_name);
        nextAnimation.curFrame = 0;
        free(bmpFile);
        break;
      }
      else
      {
        // if we land on a folder without a 0.bmp file, we need to start again
        alreadyLooped = false;
        randomCounter = (esp_random() % numAnimations) + 1;
        free(bmpFile);
      }
    }
  }
  closedir(dirPtr);

  readAnimConfig(&nextAnimation);
  
  if (!show) return;
  getNextFrame();
  if (!settings.repeatMode) esp_timer_start_once(animationTimer, curAnimation.animTime * 1000);
  sendMQTTUpdate(curAnimation.path, nextAnimation.path);
  sendWebSocketUpdate(curAnimation.path, nextAnimation.path, UPDATE_CUR_NEXT_ANIM);
}

static void printBitmapHeader(bmpHeader_t *h)
{
  ESP_LOGI(__func__, "Signature               0x%X\n", (uint16_t)h->signature);
  ESP_LOGI(__func__, "File size               %d bytes\n", (uint32_t)h->filesize);
  ESP_LOGI(__func__, "Reserved 1              0x%X\n", (uint16_t)h->reserved_0);
  ESP_LOGI(__func__, "Reserved 2              0x%X\n", (uint16_t)h->reserved_1);
  ESP_LOGI(__func__, "Bitmap data offset      0x%X\n", (uint32_t)h->offset);

  ESP_LOGI(__func__, "Info header size        %d bytes\n", (uint32_t)h->size);
  ESP_LOGI(__func__, "Bitmap width            %dpx\n", (int32_t)h->width);
  ESP_LOGI(__func__, "Bitmap height           %dpx\n", (int32_t)h->height);
  ESP_LOGI(__func__, "# color planes          %d\n", (uint16_t)h->n_cplanes);
  ESP_LOGI(__func__, "Image depth (bits / px) %d\n", (uint16_t)h->n_bpp);
  ESP_LOGI(__func__, "Compression             %d\n", (uint32_t)h->compression);
  ESP_LOGI(__func__, "Image size              %d bytes\n", (uint32_t)h->img_size);
  ESP_LOGI(__func__, "Y resolution (px / m)   %d\n", (int32_t)h->y_res);
  ESP_LOGI(__func__, "X resolution (px / m)   %d\n", (int32_t)h->x_res);
  ESP_LOGI(__func__, "# colors in palette     %d\n", (uint32_t)h->n_cpalette);
  ESP_LOGI(__func__, "# important colors      %d\n", (uint32_t)h->n_important);
}

static void indexAnimations()
{
  DIR *dirPtr;
  struct dirent* entry;
  struct stat fileStat;
  numAnimations = 0;

  remove("/sd/system/folders.lst");
  FILE *animationList = fopen("/sd/system/folders.lst", "w");
  if (animationList == NULL) return;

  dirPtr = opendir("/sd/");
  while ((entry = readdir(dirPtr)) != NULL)
  {
    if (entry->d_type == DT_DIR)
    {
      char* filePath = calloc(4 + strlen(entry->d_name) + 11 + 1, sizeof(char));
      strcpy(filePath, "/sd/");
      strcat(filePath, entry->d_name);
      strcat(filePath, "/0.bmp");

      if (stat(filePath, &fileStat) == 0)
      {
        strcpy(filePath, "/sd/");
        strcat(filePath, entry->d_name);
        strcat(filePath, "/config.ini");

        char animVersion[4];
        strcpy(animVersion, "0");

        ini_t *config = ini_load(filePath);
        if (config != NULL)
        {
          ini_sget(config, "animation", "version", "%s", &animVersion);
          ini_free(config);
        }

        fputs(entry->d_name, animationList);
        fputc('|', animationList);
        fputs(animVersion, animationList);
        fputc('\n', animationList);

        numAnimations++;
      }
      free(filePath);
    }
  };
  closedir(dirPtr);
  fclose(animationList);
}

static void drawDownloadImage()
{
  memset(pixelData, 0, sizeof(pixelColor_t) * (MATRIX_HEIGHT * MATRIX_WIDTH));

  // blue down arrow
  for (uint8_t r = 1; r <= 7; r++)
    for (uint8_t c = 14; c <= 17; c++) pixelData[r][c].blue = 255;

  pixelData[5][12].blue = 255;
  pixelData[5][13].blue = 255;
  pixelData[5][18].blue = 255;
  pixelData[5][19].blue = 255;
  pixelData[6][13].blue = 255;
  pixelData[6][18].blue = 255;
  pixelData[8][15].blue = 255;
  pixelData[8][16].blue = 255;

  // white box
  for (uint8_t r = 11; r <= 30; r++)
  {
    pixelData[r][6].color = 0xFFFFFF;
    pixelData[r][7].color = 0xFFFFFF;
    pixelData[r][24].color = 0xFFFFFF;
    pixelData[r][25].color = 0xFFFFFF;
  }

  for (uint8_t c = 8; c <= 23; c++)
  {
    pixelData[11][c].color = 0xFFFFFF;
    pixelData[12][c].color = 0xFFFFFF;
    pixelData[29][c].color = 0xFFFFFF;
    pixelData[30][c].color = 0xFFFFFF;
  }

  // random pixel colors
  for (uint8_t r = 14; r <= 26; r += 4)
  {
    for (uint8_t c = 9; c <= 22; c += 4)
    {
      uint32_t randColor = esp_random() % 0xFFFFFF;
      pixelData[r][c].color = randColor;
      pixelData[r][c + 1].color = randColor;
      pixelData[r + 1][c].color = randColor;
      pixelData[r + 1][c + 1].color = randColor;
    }
  }

  xQueueSend(ledMatrixQueue, &pixelDataPtr, portMAX_DELAY);
}

// PUBLIC FUNCTIONS ---------------------------
void drawBitmap(char *filename)
{
  bmpHeader_t header;
  bool goodBmp = true;

  // open bitmap file
  FILE *bitmapFile = fopen(filename, "r");
  if (bitmapFile == NULL)
  {
    ESP_LOGE(__func__, "Error opening bitmap %s", filename);
    return;
  }

  // read bitmap header
  if (fread(&header, sizeof(bmpHeader_t), 1, bitmapFile) != 1) goodBmp = false;

  // if bitmap meets format requirements, start reading the image data
  if (header.signature == 0x4D42                    &&  // bitmap signature
      header.n_cplanes == 1                         &&  // # planes must be 1
      header.n_bpp == 24                            &&  // must be 24 bit
      header.compression == 0                       &&  // must be uncompressed
      (header.height == 16 || header.height == 32)  &&  // must be 16 or 32 pixels height
      (header.width == 16 || header.width == 32)    &&  // must be 16 or 32 pixels width
      goodBmp)
  {
    fseek(bitmapFile, header.offset, SEEK_SET);

    // for each scanline...
    for (uint8_t row = 0; row < MATRIX_HEIGHT; row++)
    {
      // read full row at once (for speed!)
      uint8_t pixel = 0;
      uint8_t bmpPixel[3 * header.width];
      fread(bmpPixel, 1, 3 * header.width, bitmapFile);

      // for each pixel...
      for (uint8_t col = 0; col < MATRIX_WIDTH; col++)
      {
        // push to pixel buffer
        pixelData[row][col].blue = gammaCorrect(bmpPixel[pixel++]);
        pixelData[row][col].green = gammaCorrect(bmpPixel[pixel++]);
        pixelData[row][col].red = gammaCorrect(bmpPixel[pixel++]);

        // pixel double if bitmap width = 16
        if (header.width == 16)
        {
          pixelData[row][col + 1].color = pixelData[row][col].color;
          col++;
        }
      }

      // pixel double if bitmap height = 16
      if (header.height == 16)
      {
        for (uint8_t col = 0; col < MATRIX_WIDTH; col++) pixelData[row + 1][col].color = pixelData[row][col].color;
        row++;
      }
    }
  }

  fclose(bitmapFile);

  if (!goodBmp)
  {
    //displayMissingImageError(true);
    ESP_LOGE(__func__, "Error parsing bitmap %s", filename);
    printBitmapHeader(&header);
    return;
  }

  // send to LED matrix
  xQueueSend(ledMatrixQueue, &pixelDataPtr, portMAX_DELAY);
}

void director_startDirecting()
{
  directorCmd_t directorCmd;
  directorCmd.cmd = START_DIRECTING;
  xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);
}

void director_stopDirecting()
{
  directorCmd_t directorCmd;
  directorCmd.cmd = STOP_DIRECTING;
  xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);
}

void director_sleep(bool enable)
{
  directorCmd_t directorCmd;

  if (enable)
  {
    directorCmd.cmd = STOP_DIRECTING;
    xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);
    directorCmd.cmd = GOTO_SLEEP;
    xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);
  }
  else
  {
    director_startDirecting();
  }
}

void director_playNext(char* file)
{
  directorCmd_t directorCmd;
  directorCmd.cmd = PLAY_NEXT;
  strcpy(directorCmd.paramStr, file);
  xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);
}

void director_setBrightness(uint8_t brightness)
{
  directorCmd_t directorCmd;
  directorCmd.cmd = SET_BRIGHTNESS;
  directorCmd.paramInt = brightness;
  xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);
}

void director_setRepeat(bool enable)
{
  directorCmd_t directorCmd;
  directorCmd.cmd = SET_REPEAT;
  directorCmd.paramBool = enable;
  xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);
}

void director_sendUpdate(uint8_t updateBits)
{
  directorCmd_t directorCmd;
  directorCmd.cmd = SEND_UPDATE;
  directorCmd.paramInt = updateBits;
  xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);
}

void director_reindexAnims()
{
  director_stopDirecting();
  directorCmd_t directorCmd;
  directorCmd.cmd = REINDEX_ANIMS;
  xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);
  xEventGroupWaitBits(xEventGroup, DIRECTOR_CMD_OK, pdTRUE, pdFALSE, portMAX_DELAY);
  director_startDirecting();
}

void director_restart()
{
  director_stopDirecting();
  directorCmd_t directorCmd;
  directorCmd.cmd = RESTART;
  xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);
}

esp_err_t director_downloadFile(char* file)
{
  director_stopDirecting();
  directorCmd_t directorCmd;
  directorCmd.cmd = DOWNLOAD_FILE;
  strcpy(directorCmd.paramStr, file);
  xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);

  EventBits_t uxBits = xEventGroupWaitBits(xEventGroup, (DIRECTOR_CMD_OK | DIRECTOR_CMD_FAIL), pdTRUE, pdFALSE, portMAX_DELAY);
  if (uxBits & DIRECTOR_CMD_OK) return ESP_OK;
  else return ESP_FAIL;
}

esp_err_t director_deleteAnim(char* name)
{
  director_stopDirecting();
  directorCmd_t directorCmd;
  directorCmd.cmd = DELETE_ANIM;
  strcpy(directorCmd.paramStr, name);
  xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);

  EventBits_t uxBits = xEventGroupWaitBits(xEventGroup, (DIRECTOR_CMD_OK | DIRECTOR_CMD_FAIL), pdTRUE, pdFALSE, portMAX_DELAY);
  if (uxBits & DIRECTOR_CMD_OK) return ESP_OK;
  else return ESP_FAIL;
}

esp_err_t director_updateSPIFFS(char* url)
{
  director_stopDirecting();
  directorCmd_t directorCmd;
  directorCmd.cmd = UPDATE_SPIFFS;
  strcpy(directorCmd.paramStr, url);
  xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);

  EventBits_t uxBits = xEventGroupWaitBits(xEventGroup, (DIRECTOR_CMD_OK | DIRECTOR_CMD_FAIL), pdTRUE, pdFALSE, portMAX_DELAY);
  if (uxBits & DIRECTOR_CMD_OK) return ESP_OK;
  else return ESP_FAIL;
}

esp_err_t director_updateFirmware(char* url)
{
  director_stopDirecting();
  directorCmd_t directorCmd;
  directorCmd.cmd = UPDATE_FIRMWARE;
  strcpy(directorCmd.paramStr, url);
  xQueueSend(directorCmdQueue, &directorCmd, portMAX_DELAY);

  EventBits_t uxBits = xEventGroupWaitBits(xEventGroup, (DIRECTOR_CMD_OK | DIRECTOR_CMD_FAIL), pdTRUE, pdFALSE, portMAX_DELAY);
  if (uxBits & DIRECTOR_CMD_OK) return ESP_OK;
  else return ESP_FAIL;
}

void director_task(void *pvParameters)
{
  esp_timer_create_args_t timerConfig;
  timerConfig.dispatch_method = ESP_TIMER_TASK;
  timerConfig.callback = (void*)getNextFrame;
  timerConfig.arg = 0;
  timerConfig.name = "frameTimer";
  esp_timer_create(&timerConfig, &frameTimer);
  timerConfig.callback = (void*)getNextAnimation;
  timerConfig.arg = (void*)true;
  timerConfig.name = "animationTimer";
  esp_timer_create(&timerConfig, &animationTimer);

  directorCmdQueue = xQueueCreate(5, sizeof(directorCmd_t));

  ESP_LOGI(__func__, "Animation director task started on core %d", xPortGetCoreID());
  xEventGroupSetBits(xEventGroup, DIRECTOR_TASK_STARTED);

  xEventGroupWaitBits(xEventGroup, SDCARD_MOUNTED, pdFALSE, pdTRUE, portMAX_DELAY);
  indexAnimations();
  ESP_LOGI(__func__, "Found %d animations", numAnimations);

  while(1)
  {
    directorCmd_t directorCmd;
    if (xQueueReceive(directorCmdQueue, &directorCmd, portMAX_DELAY))
    {
      switch (directorCmd.cmd)
      {
        case START_DIRECTING:
          settings.sleepMode = false;
          getNextAnimation(false);
          getNextAnimation(true);
        break;

        case STOP_DIRECTING:
          esp_timer_stop(frameTimer);
          esp_timer_stop(animationTimer);
          memset(pixelData, 0, sizeof(pixelColor_t) * (MATRIX_HEIGHT * MATRIX_WIDTH));
          xQueueSend(ledMatrixQueue, &pixelDataPtr, portMAX_DELAY);
        break;

        case GOTO_SLEEP:
          if (settings.sleepMode) break;
          settings.sleepMode = true;
          sendMQTTUpdate("SYSTEM/SLEEP", "SYSTEM/SLEEP");
          sendWebSocketUpdate("SYSTEM/SLEEP", "SYSTEM/SLEEP", UPDATE_BRIGHT_MODES | UPDATE_CUR_NEXT_ANIM);
        break;

        case PLAY_NEXT:
        {
          if (directorCmd.paramStr[0] != '\0')
          {
            struct stat fileStat;
            char* bmpFile = calloc(4 + strlen(directorCmd.paramStr) + 6 + 1, sizeof(char));
            strcpy(bmpFile, "/sd/");
            strcat(bmpFile, directorCmd.paramStr);
            strcat(bmpFile, "/0.bmp");

            if (stat(bmpFile, &fileStat) == 0)
            {
              strcpy(nextAnimation.path, directorCmd.paramStr);
              readAnimConfig(&nextAnimation);
              getNextAnimation(true);
            }
            free(bmpFile);
          }
          else getNextAnimation(true);
        }
        break;

        case SET_BRIGHTNESS:
          if (directorCmd.paramInt > BRIGHTNESS_MAX) settings.brightness = BRIGHTNESS_MAX;
          else if (directorCmd.paramInt < BRIGHTNESS_MIN) settings.brightness = BRIGHTNESS_MIN;
          else settings.brightness = directorCmd.paramInt;

          sendMQTTUpdate(curAnimation.path, nextAnimation.path);
          sendWebSocketUpdate(curAnimation.path, nextAnimation.path, UPDATE_BRIGHT_MODES);
        break;

        case SET_REPEAT:
          settings.repeatMode = directorCmd.paramBool;
          if (!settings.repeatMode) esp_timer_start_once(animationTimer, curAnimation.animTime * 1000);

          sendMQTTUpdate(curAnimation.path, nextAnimation.path);
          sendWebSocketUpdate(curAnimation.path, nextAnimation.path, UPDATE_BRIGHT_MODES);
        break;

        case SEND_UPDATE:
          sendMQTTUpdate(curAnimation.path, nextAnimation.path);
          sendWebSocketUpdate(curAnimation.path, nextAnimation.path, directorCmd.paramInt);
        break;

        case REINDEX_ANIMS:
          indexAnimations();
          xEventGroupSetBits(xEventGroup, DIRECTOR_CMD_OK);
        break;

        case DOWNLOAD_FILE:
          drawDownloadImage();
          if (downloadFileToSD(directorCmd.paramStr) == ESP_OK) xEventGroupSetBits(xEventGroup, DIRECTOR_CMD_OK);
          else xEventGroupSetBits(xEventGroup, DIRECTOR_CMD_FAIL);
        break;

        case DELETE_ANIM:
          // display deleting image
          if (deleteFolderFromSD(directorCmd.paramStr) == ESP_OK) xEventGroupSetBits(xEventGroup, DIRECTOR_CMD_OK);
          else xEventGroupSetBits(xEventGroup, DIRECTOR_CMD_FAIL);
        break;

        case UPDATE_FIRMWARE:
          drawDownloadImage();
          if (otaUpdate(directorCmd.paramStr, false) == ESP_OK) xEventGroupSetBits(xEventGroup, DIRECTOR_CMD_OK);
          else xEventGroupSetBits(xEventGroup, DIRECTOR_CMD_FAIL);
        break;

        case UPDATE_SPIFFS:
          drawDownloadImage();
          if (otaUpdate(directorCmd.paramStr, true) == ESP_OK) xEventGroupSetBits(xEventGroup, DIRECTOR_CMD_OK);
          else xEventGroupSetBits(xEventGroup, DIRECTOR_CMD_FAIL);
        break;

        case RESTART:
          esp_restart();
        break;
      }
    }
  }
}

