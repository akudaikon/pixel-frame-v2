#ifndef BRIGHTNESSCTRL_H
#define BRIGHTNESSCTRL_H

#define ADC_SAMPLE_RATE_MS      50
#define ADC_SAMPLES_PER_UPDATE  15

#define BRIGHTNESS_MIN    1
#define BRIGHTNESS_MAX    32

#define LDR_MIN           50
#define LDR_MAX           400

void brightnessCtrl_task(void *pvParameters);
void brightnessCtrl_stop();

#endif