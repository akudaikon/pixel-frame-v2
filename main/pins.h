#ifndef PINS_H
#define PINS_H

#define LDR_PIN           ADC1_GPIO37_CHANNEL
#define TOUCH_PIN         GPIO_IO_4

// LED Matrix
#define LED_A_PIN         GPIO_NUM_23
#define LED_B_PIN         GPIO_NUM_32
#define LED_C_PIN         GPIO_NUM_19
#define LED_D_PIN         GPIO_NUM_26

#define LED_CLK_PIN       GPIO_NUM_22
#define LED_LAT_PIN       GPIO_NUM_25
#define LED_OE_PIN        GPIO_NUM_21  // active low

#define LED_R1_PIN        GPIO_NUM_9
#define LED_R2_PIN        GPIO_NUM_5
#define LED_G1_PIN        GPIO_NUM_27
#define LED_G2_PIN        GPIO_NUM_33
#define LED_B1_PIN        GPIO_NUM_10
#define LED_B2_PIN        GPIO_NUM_18

// SD Card (MMC)
#define SD_CMD_PIN        GPIO_NUM_15
#define SD_CLK_PIN        GPIO_NUM_14
#define SD_D0_PIN         GPIO_NUM_2
//#define SD_D1_PIN         GPIO_NUM_4
//#define SD_D2_PIN         GPIO_NUM_12
//#define SD_D3_PIN         GPIO_NUM_13

#endif