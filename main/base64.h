#ifndef BASE64_H
#define BASE64_H

int base64_encode_file(FILE *infile, char *out, size_t out_len);

#endif