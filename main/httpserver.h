#ifndef HTTPSERVER_H
#define HTTPSERVER_H

void httpServer_task(void *pvParameters);
void sendWebSocketUpdate(char* curAnim, char* nextAnim, uint8_t updateBits);

#endif